package tools;

import bindead.analyses.Analysis;
import bindead.analyses.fixpoint.CallString;
import bindead.analyses.fixpoint.TransitionSystem;
import binparse.Binary;
import binparse.BinaryFileFormat;
import binparse.Symbol;
import java.io.IOException;
import java.util.Map;
import javalx.data.Option;
import rreil.lang.RReilAddr;
import ycomb.p9.binary.analysis.Analyzer;
import ycomb.p9.binary.cfg.controller.NativeContextMenu;
import ycomb.p9.binary.cfg.controller.RReilContextMenu;
import ycomb.p9.binary.cfg.display.CfgComponent;
import ycomb.p9.binary.cfg.display.CfgView;
import ycomb.p9.binary.cfg.display.renderer.RReilInstructionRenderer;
import ycomb.p9.binary.cfg.display.renderer.X86InstructionRenderer;
import ycomb.p9.binary.cfg.graph.AnalysisResult;
import ycomb.p9.binary.cfg.graph.BasicBlockTransformation;
import ycomb.p9.binary.cfg.graph.Cfg;
import ycomb.p9.binary.cfg.graph.CfgBuilder;

/**
 * Collection of tools to make writing tests easier.
 *
 * @author Bogdan Mihaila
 */
public class TestsHelper {

  public final static String binariesPath32bit = "/binaries/x86_32/";
  public final static String binariesPath64bit = "/binaries/x86_64/";

  /**
   * Instantiate a binary from a resource file path. Automatically chooses the
   * right binary format (LINUX, Windows, RREIL, etc.).
   *
   * @param resourcePath A file path that is relative to this projects 32 bit
   * binary examples path.
   * @return The binary
   * @throws IOException if there was an error while trying to access the file
   * @see #binariesPath32bit
   */
  public static Binary get32bitExamplesBinary(String resourcePath) throws IOException {
    String file = TestsHelper.class.getResource(binariesPath32bit + resourcePath).getPath();
    return BinaryFileFormat.getBinary(file);
  }

  /**
   * Instantiate a binary from a resource file path. Automatically chooses the
   * right binary format (LINUX, Windows, RREIL, etc.).
   *
   * @param resourcePath A file path that is relative to this projects 64 bit
   * binary examples path.
   * @return The binary
   * @throws IOException if there was an error while trying to access the file
   * @see #binariesPath64bit
   */
  public static Binary get64bitExamplesBinary(String resourcePath) throws IOException {
    String file = TestsHelper.class.getResource(binariesPath64bit + resourcePath).getPath();
    return BinaryFileFormat.getBinary(file);
  }

  public static AnalysisResult analyze(Binary binary) {
    Analyzer analyzer = new Analyzer();
    Analysis<?> analysis = analyzer.runAnalysis(binary);
    AnalysisResult result = new AnalysisResult(analysis, binary.getFileName());
    return result;
  }

  private static CfgView buildNativeCfg(AnalysisResult analysis, CallString callstring) {
    X86InstructionRenderer instructionRenderer = new X86InstructionRenderer();
    Cfg function = BasicBlockTransformation.run(new CfgBuilder(analysis, callstring).nativeCfg());
    CfgView view = new CfgView(function, instructionRenderer);
    view.addControlListener(new NativeContextMenu(function, Cfg.getName(analysis, callstring), Cfg.$Block));
    return view;
  }

  private static CfgView buildRReilCfg(AnalysisResult analysis, CallString callstring) {
    RReilInstructionRenderer instructionRenderer = new RReilInstructionRenderer();
    instructionRenderer.disableOpcode(true);
    Cfg function = BasicBlockTransformation.run(new CfgBuilder(analysis, callstring).rreilCfg());
    CfgView view = new CfgView(function, instructionRenderer);
    view.addControlListener(new RReilContextMenu(function, Cfg.getName(analysis, callstring), Cfg.$Block));
    return view;
  }

  /**
   * Return a bare bones RREIL CFG component build up from a Prefuse view.
   */
  public static CfgView buildRReilCfg(AnalysisResult result, String functionName) {
    return buildRReilCfg(result, getSomeCallStringForFunction(result, functionName));
  }

  /**
   * Return a bare bones Native CFG component build up from a Prefuse view.
   */
  public static CfgView buildNativeCfg(AnalysisResult result, String functionName) {
    return buildNativeCfg(result, getSomeCallStringForFunction(result, functionName));
  }

  /**
   * Return the RREIL CFG component as used in the GUI tabs.
   */
  public static CfgComponent buildRReilCfgComponent(AnalysisResult result, String functionName) {
    return CfgComponent.buildRReilCfg(result, getSomeCallStringForFunction(result, functionName));
  }

  /**
   * Return the Native CFG component as used in the GUI tabs.
   */
  public static CfgComponent buildNativeCfgComponent(AnalysisResult result, String functionName) {
    return CfgComponent.buildNativeCfg(result, getSomeCallStringForFunction(result, functionName));
  }

  /**
   * Return the first found call-string in the analysis for a function name.
   */
  public static CallString getSomeCallStringForFunction(AnalysisResult result, String functionName) {
    Binary binary = result.getAnalysis().getBinaryCode().getBinary();
    Option<Symbol> symbol = binary.getSymbol(functionName);
    if (symbol.isNone()) {
      throw new IllegalArgumentException("No function with name " + functionName + " found.");
    }
    long functionAddress = symbol.get().getAddress();
    Map<CallString, TransitionSystem.ProceduralTransitions> procedures = result.getAnalysis().getTransitionSystem().getAllProcedures();
    for (Map.Entry<CallString, TransitionSystem.ProceduralTransitions> entry : procedures.entrySet()) {
      CallString.Transition transition = entry.getKey().peek();
      RReilAddr callToCfg = transition.getTarget(); // a procedure is identified by the call entry it has, thus target here
      if (callToCfg.base() == functionAddress) {
        return entry.getKey();
      }
    }
    throw new IllegalArgumentException("No function with name " + functionName + " found.");
  }

}
