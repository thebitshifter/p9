package graphs;

import binparse.Binary;
import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.io.IOException;
import javalx.data.products.P2;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import tools.TestsHelper;
import ycomb.p9.binary.cfg.display.CfgComponent;
import ycomb.p9.binary.cfg.graph.AnalysisResult;

/**
 * Test various components of the CFG display.
 *
 * @author Bogdan Mihaila
 */
public class LayoutTests {

  public static void main(String... args) throws IOException {
    crackaddr();
  }

  /**
   * Analyze a binary from a given filename and return the Native and RREIL CFGs
   * for the given function name.
   */
  public static P2<CfgComponent, CfgComponent> analyzeAndBuildCfgs(String fileName, String functionName) throws IOException {
    Binary binary = TestsHelper.get32bitExamplesBinary(fileName);
    AnalysisResult result = TestsHelper.analyze(binary);
    CfgComponent nativeCfg = TestsHelper.buildNativeCfgComponent(result, functionName);
    CfgComponent rreilCfg = TestsHelper.buildRReilCfgComponent(result, functionName);
    return P2.tuple2(nativeCfg, rreilCfg);
  }

  public static void crackaddr() throws IOException {
    Binary binary = TestsHelper.get32bitExamplesBinary("crackaddr-good-oversimplified");
    AnalysisResult result = TestsHelper.analyze(binary);
//    CfgView cfg = TestsHelper.buildRReilCfg(result, "copy_it");
    CfgComponent cfg = TestsHelper.buildRReilCfgComponent(result, "copy_it");
    showWindowWithGraph(cfg);
  }

  public static void showWindowWithGraph(final JComponent cfg) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        // TODO: old code that also added the search panel. See how to reactivate
//        JComponent body = new JLayeredPane();
//        body.add(cfg, Integer.valueOf(0));
//
//        Box box = new Box(BoxLayout.X_AXIS);
//        box.add(Box.createHorizontalGlue());
//    JComponent searchPanel = searcher.buildSearchPanel();
//    int searchPanelHeight = searchPanel.getPreferredSize().height;
//    box.add(searchPanel);
//    box.setBounds(0, bounds.height - searchPanelHeight - 30, bounds.width, searchPanelHeight);
//        body.add(box, Integer.valueOf(1));
//        body.setBackground(ColorConfig.$Background);
//        body.setForeground(ColorConfig.$Foreground);
//
        JFrame frame = new JFrame("CFG Example");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Rectangle bounds = new Rectangle(0, 0, 1000, 800);

//        frame.getContentPane().add(body, BorderLayout.CENTER);
//        cfg.setSize(1000, 800); // or setbounds?
        frame.getContentPane().add(cfg, BorderLayout.CENTER);
        frame.pack();
        frame.setSize(bounds.getSize());
        frame.setVisible(true);
      }
    });
  }
}
