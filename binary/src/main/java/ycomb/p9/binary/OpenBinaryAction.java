package ycomb.p9.binary;

import bindead.analyses.Analysis;
import bindead.analyses.fixpoint.CallString;
import bindead.exceptions.CallStringAnalysisException;
import binparse.Binary;
import binparse.BinaryFactory;
import java.awt.event.ActionEvent;
import java.io.IOException;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.awt.ActionID;
import org.openide.awt.ActionRegistration;
import org.openide.loaders.DataObject;
import org.openide.util.Cancellable;
import org.openide.util.ContextAwareAction;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.util.actions.ActionInvoker;
import org.openide.util.actions.Presenter;
import rreil.lang.RReilAddr;
import ycomb.p9.binary.analysis.Analyzer;
import ycomb.p9.binary.cfg.controller.events.CfgViewEventBus;
import ycomb.p9.binary.cfg.controller.events.CfgViewEventBus.CfgFocusAddressEvent;
import ycomb.p9.binary.cfg.display.CallTreeComponent;
import ycomb.p9.binary.cfg.display.CfgComponent;
import ycomb.p9.binary.cfg.graph.AnalysisResult;

@ActionID(category = "File", id = "ycomb.p9.binary.OpenBinaryAction")
@ActionRegistration(lazy = false, iconBase = OpenBinaryAction.iconPath, displayName = "#CTL_OpenBinaryAction")
@Messages({
  "CTL_OpenBinaryAction=Analyze",
  "HINT_OpenBinaryAction=Reconstruct the CFGs for this file."
})
public class OpenBinaryAction extends AbstractAction implements ContextAwareAction, Presenter.Popup {
  protected static final String iconPath = CfgComponent.iconPath;
  private static final ImageIcon icon = ImageUtilities.loadImageIcon(iconPath, false);
  private Binary binary;

  // no argument constructor necessary for context aware actions
  public OpenBinaryAction () {
    this(null);
  }

  public OpenBinaryAction (Binary binary) {
    this.binary = binary;
    init();
  }

  public OpenBinaryAction (DataObject dataObject, BinaryFactory factory) {
    try {
      binary = getBinary(dataObject.getPrimaryFile().getPath(), factory);
      init();
    } catch (IOException e) {
      Exceptions.printStackTrace(e);
      binary = null;
      setEnabled(false);
    }
  }

  private void init () {
    setEnabled(binary != null);
    putValue(Action.SMALL_ICON, icon);
    putValue(Action.NAME, Bundle.CTL_OpenBinaryAction());
    putValue(Action.SHORT_DESCRIPTION, Bundle.HINT_OpenBinaryAction());
  }

  @Override public void actionPerformed (ActionEvent e) {
    ActionInvoker.invokeAction(null, e, true, new Runnable() {
      @Override public void run () {
        performAction();
      }
    });
  }

  @Override public Action createContextAwareInstance (Lookup actionContext) {
    Binary parameter = actionContext.lookup(Binary.class);
    return new OpenBinaryAction(parameter);
  }

  @Override public JMenuItem getPopupPresenter () {
    return new JMenuItem(this);
  }

  private void performAction () {
    final Thread currentThread = Thread.currentThread();
    ProgressHandle progressBar = ProgressHandleFactory.createHandle("Reconstruction", new Cancellable() {
      @Override public boolean cancel () {
        currentThread.interrupt();
        return true;
      }
    });
    progressBar.start();
    progressBar.switchToIndeterminate();
    progressBar.setDisplayName("Reconstructing CFGs ...");
    try {
      analyze();
    } catch (CallStringAnalysisException e) {
      Exceptions.printStackTrace(e);
      final String fileName = binary.getFileName();
      final AnalysisResult analysis = new AnalysisResult(e.getAnalysis(), fileName);
      final CallString callString = e.getCallString();
      final RReilAddr address = e.getAddress();
      SwingUtilities.invokeLater(new Runnable() {
        @Override public void run () {
          Action openCallTreeAction = CallTreeComponent.getOpenAction(analysis, fileName);
          openCallTreeAction.actionPerformed(null);
          Action openRReilCfgAction = CfgComponent.getOpenRReilCfgAction(analysis, callString);
          openRReilCfgAction.actionPerformed(null);
          String cfgID = CfgViewEventBus.getRReilEventsID(analysis, callString);
          CfgFocusAddressEvent event = new CfgFocusAddressEvent(cfgID, address, null);
          CfgViewEventBus.getInstance().publish(event);
        }
      });
    } catch (Exception e) {
      Exceptions.printStackTrace(e);
    } finally {
      progressBar.finish();
    }
  }

  private void analyze () {
    if (binary != null)
      performReconstruction(binary);
  }

  private static void performReconstruction (Binary binary) {
    final AnalysisResult result = reconstruct(binary);
    final String fileName = binary.getFileName();
    SwingUtilities.invokeLater(new Runnable() {
      @Override public void run () {
        Action openCallTreeAction = CallTreeComponent.getOpenAction(result, fileName);
        openCallTreeAction.actionPerformed(null);
      }
    });
  }

  private static AnalysisResult reconstruct (Binary binary) {
    Analyzer analyzer = new Analyzer();
    Analysis<?> analysis = analyzer.runAnalysis(binary);
    AnalysisResult result = new AnalysisResult(analysis, binary.getFileName());
    return result;
  }

  private static Binary getBinary (String filePath, BinaryFactory factory) throws IOException {
    return factory.getBinary(filePath);
  }

}
