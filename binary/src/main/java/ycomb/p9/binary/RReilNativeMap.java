package ycomb.p9.binary;

import java.util.HashMap;
import java.util.Map;

import rreil.lang.RReilAddr;

public class RReilNativeMap<T> {
  private final Map<RReilAddr, T> rreilMap = new HashMap<RReilAddr, T>();
  private final Map<Long, T> nativeMap = new HashMap<Long, T>();

  public T getNative (RReilAddr address) {
    return nativeMap.get(address.base());
  }

  public T getRReil (RReilAddr address) {
    return rreilMap.get(address);
  }

  public int size () {
    return rreilMap.size();
  }

  public boolean isEmpty () {
    return rreilMap.isEmpty();
  }

  public boolean containsRReilKey (RReilAddr key) {
    return rreilMap.containsKey(key);
  }

  public boolean containsNativeKey (RReilAddr key) {
    return nativeMap.containsKey(key.base());
  }

  public void put (RReilAddr key, T value) {
    rreilMap.put(key, value);
    nativeMap.put(key.base(), value);
  }

}
