package ycomb.p9.binary;

import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class WindowsUtils {

  public static void dockInMode (String modeName, TopComponent component) {
    Mode mode = WindowManager.getDefault().findMode(modeName);
    if (mode != null)
      mode.dockInto(component);
  }

}
