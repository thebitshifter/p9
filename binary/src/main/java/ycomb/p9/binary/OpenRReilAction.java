package ycomb.p9.binary;

import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.filesystems.FileObject;
import org.openide.util.Cancellable;
import org.openide.util.ContextAwareAction;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.util.actions.ActionInvoker;
import org.openide.util.actions.Presenter;

import rreil.lang.RReilAddr;
import ycomb.p9.binary.analysis.Analyzer;
import ycomb.p9.binary.cfg.controller.events.CfgViewEventBus;
import ycomb.p9.binary.cfg.controller.events.CfgViewEventBus.CfgFocusAddressEvent;
import ycomb.p9.binary.cfg.display.CallTreeComponent;
import ycomb.p9.binary.cfg.display.CfgComponent;
import ycomb.p9.binary.cfg.graph.AnalysisResult;
import bindead.analyses.Analysis;
import bindead.analyses.fixpoint.CallString;
import bindead.exceptions.CallStringAnalysisException;
import binparse.rreil.RReilBinary;

@ActionID(category = "File", id = "ycomb.p9.binary.OpenRReilAction")
@ActionRegistration(lazy = false, iconBase = CfgComponent.iconPath, displayName = "#CTL_OpenRReilAction")
@ActionReferences({
    @ActionReference(path = "Loaders/text/x-rreil/Actions", position = 0)})
@Messages({
    "CTL_OpenRReilAction=Analyze",
    "HINT_OpenRReilAction=Reconstruct the CFGs for this file."
})
public class OpenRReilAction extends AbstractAction implements ContextAwareAction, Presenter.Popup {

    protected static final String iconPath = CfgComponent.iconPath;
    private static final ImageIcon icon = ImageUtilities.loadImageIcon(iconPath, false);
    private final FileObject file;

    // no argument constructor necessary for context aware actions
    public OpenRReilAction() {
        this(null);
    }

    public OpenRReilAction(RReilFileDataObject dataObject) {
        file = dataObject != null ? dataObject.getPrimaryFile() : null;
        init();
    }

    private void init() {
        setEnabled(file != null);
        putValue(Action.SMALL_ICON, icon);
        putValue(Action.NAME, Bundle.CTL_OpenRReilAction());
        putValue(Action.SHORT_DESCRIPTION, Bundle.HINT_OpenRReilAction());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ActionInvoker.invokeAction(null, e, true, new Runnable() {
            @Override
            public void run() {
                performAction();
            }
        });
    }

    @Override
    public Action createContextAwareInstance(Lookup actionContext) {
        RReilFileDataObject fileObject = actionContext.lookup(RReilFileDataObject.class);
        return new OpenRReilAction(fileObject);
    }

    @Override
    public JMenuItem getPopupPresenter() {
        return new JMenuItem(this);
    }

    private void performAction() {
        final Thread currentThread = Thread.currentThread();
        ProgressHandle progressBar = ProgressHandleFactory.createHandle("Reconstruction", new Cancellable() {
            @Override
            public boolean cancel() {
                currentThread.interrupt();
                return true;
            }
        });
        progressBar.start();
        progressBar.switchToIndeterminate();
        progressBar.setDisplayName("Reconstructing CFGs ...");
        try {
            analyze();
        } catch (CallStringAnalysisException e) {
            Exceptions.printStackTrace(e);
            final String fileName = file.getName();
            final AnalysisResult analysis = new AnalysisResult(e.getAnalysis(), fileName);
            final CallString callString = e.getCallString();
            final RReilAddr address = e.getAddress();
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    Action openCallTreeAction = CallTreeComponent.getOpenAction(analysis, fileName);
                    openCallTreeAction.actionPerformed(null);
                    Action openRReilCfgAction = CfgComponent.getOpenRReilCfgAction(analysis, callString);
                    openRReilCfgAction.actionPerformed(null);
                    String cfgID = CfgViewEventBus.getRReilEventsID(analysis, callString);
                    CfgFocusAddressEvent event = new CfgFocusAddressEvent(cfgID, address, null);
                    CfgViewEventBus.getInstance().publish(event);
                }
            });
        } catch (Exception e) {
            Exceptions.printStackTrace(e);
        } finally {
            progressBar.finish();
        }
    }

    private void analyze() throws IOException {
        if (file != null)
            performReconstruction(file);
    }

    private static void performReconstruction(FileObject file) throws IOException {
        final AnalysisResult result = reconstruct(file);
        final String fileName = file.getNameExt();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Action openCallTreeAction = CallTreeComponent.getOpenAction(result, fileName);
                openCallTreeAction.actionPerformed(null);
            }
        });
    }

    private static AnalysisResult reconstruct(FileObject file) throws IOException {
        Analyzer analyzer = new Analyzer();
        Analysis<?> analysis = analyzer.runAnalysis(new RReilBinary(file.getPath()));
        AnalysisResult result = new AnalysisResult(analysis, file.getName());
        return result;
    }

}
