package ycomb.p9.binary.values;

import bindead.domainnetwork.interfaces.RootDomain;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.core.spi.multiview.CloseOperationState;
import org.netbeans.core.spi.multiview.MultiViewDescription;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.MultiViewElementCallback;
import org.netbeans.core.spi.multiview.MultiViewFactory;
import org.netbeans.lib.editor.util.ListenerList;
import org.openide.awt.UndoRedo;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.util.lookup.Lookups;
import org.openide.windows.TopComponent;
import rreil.lang.RReilAddr;
import rreil.lang.Rhs.Rvar;
import ycomb.p9.binary.WindowsUtils;

@Messages({
  "CTL_StateComponentAction=For all known variables",
  "CTL_StateComponentVariablesAction=Only for variables used in the instruction",
  "HINT_StateComponentAction=Display the state at this program point.",
  "HINT_StateComponentVariablesAction=Display the state at this program point.",
  "# parameter {0} is the address of the state",
  "CTL_StateComponentTitle=State at {0}",
  "HINT_StateComponent=State at address {0}",
  "CTL_StateCloneAction=Take permanent snapshot",
  "HINT_StateCloneAction=Clone this view to show the state permanently.",
  "HINT_RegexInput=Type a regular expression to filter the shown variables by name."
})
/**
 * A multiple views display to show an analysis state value.
 */
public class AnalysisStateDisplay {
  private static final String iconPath = "ycomb/p9/binary/icons/view-form-table.png";
  private static final ImageIcon icon = ImageUtilities.loadImageIcon(iconPath, false);
  private static final Image iconImage = ImageUtilities.loadImage(iconPath);
  private static String emptyStateMessage = "Nothing to see here, move on.";
  protected final ListenerList<ChangeListener> changeListeners = new ListenerList<ChangeListener>();
  private AnalysisStateWrapper analysisState;
  protected RReilAddr address;
  protected String regexFilter;
  private static AnalysisStateDisplay updatableDisplay;

  private AnalysisStateDisplay (AnalysisStateWrapper analysisState, RReilAddr address, String regexFilter) {
    this.analysisState = analysisState;
    this.address = address;
    this.regexFilter = regexFilter;
  }

  public static Action getShowStateSomeVariablesAction (final RootDomain<?> state, final RReilAddr address, Set<Rvar> variables) {
    AnalysisStateDisplay stateDisplay = new AnalysisStateDisplay(new AnalysisStateWrapper(state), address, buildRegexFromVariables(variables));
    Action action = stateDisplay.getOpenAction();
    action.putValue(Action.NAME, Bundle.CTL_StateComponentVariablesAction());
    action.putValue(Action.SHORT_DESCRIPTION, Bundle.HINT_StateComponentVariablesAction());
    return action;
  }

  public static Action getShowStateAllVariablesAction (final RootDomain<?> state, final RReilAddr address) {
    AnalysisStateDisplay stateDisplay = new AnalysisStateDisplay(new AnalysisStateWrapper(state), address, ".*");
    Action action = stateDisplay.getOpenAction();
    action.putValue(Action.NAME, Bundle.CTL_StateComponentAction());
    action.putValue(Action.SHORT_DESCRIPTION, Bundle.HINT_StateComponentAction());
    return action;
  }

  private Action getOpenAction () {
    Action action = new AbstractAction() {
      @Override public void actionPerformed (ActionEvent e) {
        if (updatableDisplay == null) {
          TopComponent display = getDisplayComponent(AnalysisStateDisplay.this, 0);
          display.open();
        } else {
          refreshUpdatableDisplay(AnalysisStateDisplay.this);
        }
      }
    };
    return action;
  }

  private static TopComponent getDisplayComponent (AnalysisStateDisplay stateDisplay, int multiviewIndex) {
    TopComponent component;
    MultiViewDescription descriptions[] =
    {new HtmlPanelDescription(stateDisplay, 0), new XmlPanelDescription(stateDisplay, 1),
      new TextPanelDescription(stateDisplay, 2), new CompactTextPanelDescription(stateDisplay, 3)};
    component = MultiViewFactory.createMultiView(descriptions, descriptions[multiviewIndex]);
    WindowsUtils.dockInMode("domain", component);
    return component;
  }

  private static String buildRegexFromVariables (Set<Rvar> variables) {
    StringBuilder builder = new StringBuilder();
    builder.append("(?x) "); // an embedded regex flag to ignore whitespace
    for (Rvar variable : variables) {
      String name = variable.toString();
      builder.append(name.substring(0, name.indexOf(':')));
      builder.append(" | ");
    }
    builder.setLength(builder.length() - 3); // remove last "|" character
    return builder.toString();
  }

  private static void refreshDisplay (AnalysisStateDisplay display) {
    for (ChangeListener listener : display.changeListeners.getListeners()) {
      listener.stateChanged(null);
    }
  }

  private static void refreshUpdatableDisplay (AnalysisStateDisplay newValues) {
    updatableDisplay.analysisState = newValues.analysisState;
    updatableDisplay.address = newValues.address;
    updatableDisplay.regexFilter = newValues.regexFilter;
    refreshDisplay(updatableDisplay);
  }

  private static void setUpdatableDisplay (AnalysisStateDisplay stateDisplay) {
    if (updatableDisplay == null)
      updatableDisplay = stateDisplay;
  }

  private static boolean isUpdatableDisplay (AnalysisStateDisplay stateDisplay) {
    return updatableDisplay == stateDisplay;
  }

  private static void removeUpdatableDisplay () {
    updatableDisplay = null;
  }

  private static abstract class MultiViewPanelDescription implements MultiViewDescription, Serializable {
    private final Image icon = iconImage;
    protected final AnalysisStateDisplay ctx;
    protected final int componentIndex;
    protected String name;
    protected String id;

    public MultiViewPanelDescription (AnalysisStateDisplay ctx, int componentIndex) {
      this.ctx = ctx;
      this.componentIndex = componentIndex;
    }

    @Override public int getPersistenceType () {
      return TopComponent.PERSISTENCE_NEVER;
    }

    @Override public String getDisplayName () {
      return name;
    }

    @Override public Image getIcon () {
      return icon;
    }

    @Override public HelpCtx getHelpCtx () {
      return HelpCtx.DEFAULT_HELP;
    }

    @Override public String preferredID () {
      return id;
    }

  }

  private static class HtmlPanelDescription extends MultiViewPanelDescription {
    private final int componentIndex;

    public HtmlPanelDescription (AnalysisStateDisplay stateDisplay, int componentIndex) {
      super(stateDisplay, componentIndex);
      this.componentIndex = componentIndex;
      name = "Html";
      id = "STATE_PANEL_HTML";
    }

    @Override public MultiViewElement createElement () {
      return new HtmlPanel(ctx, componentIndex);
    }
  }

  private static class XmlPanelDescription extends MultiViewPanelDescription {
    public XmlPanelDescription (AnalysisStateDisplay stateDisplay, int componentIndex) {
      super(stateDisplay, componentIndex);
      name = "Xml";
      id = "STATE_PANEL_XML";
    }

    @Override public MultiViewElement createElement () {
      return new XmlPanel(ctx, componentIndex);
    }
  }

  private static class TextPanelDescription extends MultiViewPanelDescription {
    public TextPanelDescription (AnalysisStateDisplay stateDisplay, int componentIndex) {
      super(stateDisplay, componentIndex);
      name = "Text";
      id = "STATE_PANEL_TEXT";
    }

    @Override public MultiViewElement createElement () {
      return new TextPanel(ctx, componentIndex);
    }
  }

  private static class CompactTextPanelDescription extends MultiViewPanelDescription {
    public CompactTextPanelDescription (AnalysisStateDisplay stateDisplay, int componentIndex) {
      super(stateDisplay, componentIndex);
      name = "Compact Text";
      id = "STATE_PANEL_COMPACT_TEXT";
    }

    @Override public MultiViewElement createElement () {
      return new CompactTextPanel(ctx, componentIndex);
    }
  }

  private static abstract class MultiViewPanel implements MultiViewElement {
    private MultiViewElementCallback callback = null;
    protected JComponent thisPanelViewComponent;
    private final JToolBar toolbar;
    private JTextField regexInput;
    private final AnalysisStateDisplay ctx;

    public MultiViewPanel (AnalysisStateDisplay ctx, int componentIndex) {
      this.ctx = ctx;
      toolbar = initToolbar(componentIndex);
    }

    protected abstract void setContent (AnalysisStateWrapper state, String regexFilter);

    protected void setTitle (RReilAddr address) {
      String updatableDisplayMarker = isUpdatableDisplay(ctx) ? "* " : "";
      String addressString = "0x" + address;
      String titleString = updatableDisplayMarker + Bundle.CTL_StateComponentTitle(addressString);
      callback.updateTitle(titleString);
      TopComponent display = callback.getTopComponent();
      display.setDisplayName(titleString);
      display.setToolTipText(Bundle.HINT_StateComponent(addressString));
    }

    private JToolBar initToolbar (final int componentIndex) {
      Action clone = new AbstractAction() {
        @Override public void actionPerformed (ActionEvent e) {
          AnalysisStateDisplay stateDisplay = new AnalysisStateDisplay(ctx.analysisState, ctx.address, ctx.regexFilter);
          TopComponent display = getDisplayComponent(stateDisplay, componentIndex);
          display.open();
        }
      };
      clone.putValue(Action.SMALL_ICON, icon);
      clone.putValue(Action.NAME, Bundle.CTL_StateCloneAction());
      clone.putValue(Action.SHORT_DESCRIPTION, Bundle.HINT_StateCloneAction());
      JToolBar toolbar = new JToolBar();
      toolbar.addSeparator();
      regexInput = new JTextField(10);
      regexInput.setToolTipText(Bundle.HINT_RegexInput());
      regexInput.setMaximumSize(new Dimension(Integer.MAX_VALUE, 20));
      regexInput.addActionListener(new ActionListener() {
        @Override public void actionPerformed (ActionEvent e) {
          String regex = regexInput.getText() + ".*"; // by default the end of the string should not matter
          ctx.regexFilter = regex;
          refreshDisplay(ctx);
        }
      });
      toolbar.add(new JLabel("Filter: "));
      toolbar.add(regexInput);
      toolbar.addSeparator();
      toolbar.add(Box.createHorizontalGlue());
      toolbar.add(clone);
      toolbar.addSeparator(new Dimension(20, 0));
      return toolbar;
    }

    private void updateFilterTextfield () {
      String regex = ctx.regexFilter;
      if (regex.endsWith(".*")) // do not show the default ending
        regex = regex.substring(0, regex.length() - 2);
      regexInput.setText(regex);
    }

    @Override public JComponent getVisualRepresentation () {
      return thisPanelViewComponent;
    }

    @Override public JComponent getToolbarRepresentation () {
      return toolbar;
    }

    @Override public Action[] getActions () {
      if (callback != null) {
        return callback.createDefaultActions();
      } else {
        return new Action[] {};
      }
    }

    @Override public Lookup getLookup () {
      return Lookups.fixed(ctx);
    }

    @Override public void componentOpened () {
      setUpdatableDisplay(ctx);
      setContent(ctx.analysisState, ctx.regexFilter);
      setTitle(ctx.address);
      ctx.changeListeners.add(new ChangeListener() {
        @Override public void stateChanged (ChangeEvent e) {
          MultiViewPanel.this.setContent(ctx.analysisState, ctx.regexFilter);
          MultiViewPanel.this.setTitle(ctx.address);
          updateFilterTextfield();
        }
      });
    }

    @Override public void componentClosed () {
      if (isUpdatableDisplay(ctx))
        removeUpdatableDisplay();
    }

    @Override public void componentShowing () {
      updateFilterTextfield();
    }

    @Override public void componentHidden () {
    }

    @Override public void componentActivated () {
    }

    @Override public void componentDeactivated () {
    }

    @Override public UndoRedo getUndoRedo () {
      return UndoRedo.NONE;
    }

    @Override public void setMultiViewCallback (MultiViewElementCallback callback) {
      this.callback = callback;
    }

    @Override public CloseOperationState canCloseElement () {
      return CloseOperationState.STATE_OK;
    }

  }

  private static class HtmlPanel extends MultiViewPanel {
    private final JTextPane display;

    public HtmlPanel (AnalysisStateDisplay ctx, int componentIndex) {
      super(ctx, componentIndex);
      display = new JTextPane();
      display.setEditable(false);
      display.setContentType("text/html");
      thisPanelViewComponent = new JScrollPane(display);
    }

    @Override protected void setContent (AnalysisStateWrapper state, String regexFilter) {
      if (state == null) {
        display.setText(emptyStateMessage);
      } else {
        display.setText(state.toHTMLDumpFiltered(regexFilter));
      }
      display.setCaretPosition(0);
    }
  }

  private static class XmlPanel extends MultiViewPanel {
    private final JTextArea display;

    public XmlPanel (AnalysisStateDisplay ctx, int componentIndex) {
      super(ctx, componentIndex);
      display = new JTextArea();
      display.setLineWrap(false);
      display.setEditable(false);
      thisPanelViewComponent = new JScrollPane(display);
    }

    @Override protected void setContent (AnalysisStateWrapper state, String regexFilter) {
      if (state == null)
        display.setText(emptyStateMessage);
      else
        display.setText(state.toXMLDump());
      display.setCaretPosition(0);
    }
  }

  private static class TextPanel extends MultiViewPanel {
    private final JTextArea display;

    public TextPanel (AnalysisStateDisplay ctx, int componentIndex) {
      super(ctx, componentIndex);
      display = new JTextArea();
      display.setLineWrap(false);
      display.setEditable(false);
      display.setFont(Font.decode(Font.MONOSPACED));
      thisPanelViewComponent = new JScrollPane(display);
    }

    @Override protected void setContent (AnalysisStateWrapper state, String regexFilter) {
      if (state == null)
        display.setText(emptyStateMessage);
      else
        display.setText(state.toTextDump());
      display.setCaretPosition(0);
    }
  }

  private static class CompactTextPanel extends MultiViewPanel {
      private final JTextArea display;

      public CompactTextPanel (AnalysisStateDisplay ctx, int componentIndex) {
        super(ctx, componentIndex);
        display = new JTextArea();
        display.setLineWrap(false);
        display.setEditable(false);
        display.setFont(Font.decode(Font.MONOSPACED));
        thisPanelViewComponent = new JScrollPane(display);
      }

      @Override protected void setContent (AnalysisStateWrapper state, String regexFilter) {
        if (state == null) {
          display.setText(emptyStateMessage);
        } else {
          display.setText(state.toCompactTextDump());
        }
        display.setCaretPosition(0);
      }
    }

}
