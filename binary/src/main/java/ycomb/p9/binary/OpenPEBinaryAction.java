package ycomb.p9.binary;

import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.loaders.DataObject;
import org.openide.util.NbBundle.Messages;

import ycomb.p9.binary.cfg.display.CfgComponent;
import binparse.pe.PEBinary;

@ActionID(category = "File", id = "ycomb.p9.binary.OpenPEBinaryAction")
@ActionRegistration(lazy = false, iconBase = CfgComponent.iconPath, displayName = "#CTL_OpenPEBinaryAction")
@ActionReferences({@ActionReference(path = "Loaders/application/octet-stream/Actions", position = 0)})
@Messages("CTL_OpenPEBinaryAction=Analyze")
public final class OpenPEBinaryAction extends OpenBinaryAction {

  public OpenPEBinaryAction () {
    super(null);
  }

  public OpenPEBinaryAction (DataObject context) {
    super(context, PEBinary.getFactory());
  }

}
