package ycomb.p9.binary.analysis;

import rreil.assembler.CompiledAssembler;
import rreil.lang.RReilAddr;
import bindead.analyses.Analysis;
import bindead.analyses.AnalysisFactory;
import bindead.analyses.systems.LinuxX86Model;
import bindead.analyses.systems.SystemModelRegistry;
import bindead.environment.AnalysisEnvironment;
import bindead.environment.platform.RReilPlatform;
import binparse.Binary;
import binparse.rreil.RReilBinary;

/**
 * An utility class to initialize and run analyses.
 *
 * @author Bogdan Mihaila
 */
public class Analyzer {
  private final String domainHierarchy;
  private static final String defaultDomainHierarchy =
    "SegMem Processor Stack -Null Data -Heap Fields Predicates(F) -Undef PointsTo "
      + "Wrapping DelayedWidening -Phased ThresholdsWidening "
      + "Predicates(Z) Affine Congruences Intervals -IntervalSets";

  /**
   * Instantiate the analysis with the default domain hierarchy.
   */
  public Analyzer () {
    this(defaultDomainHierarchy);
  }

  /**
   * Instantiate the analysis with the domain hierarchy given by {@code domainHierarchy}.
   *
   * @param domainHierarchy A string to be parsed and instantiated to a domain hierarchy.
   * @see DomainFactory.parseFactory() for a description of the syntax.
   */
  public Analyzer (String domainHierarchy) {
    this.domainHierarchy = domainHierarchy;
  }

  @SuppressWarnings({"unchecked", "rawtypes"}) public Analysis<?> runAnalysis (Binary binary) {
    LinuxX86Model systemModel = SystemModelRegistry.getLinuxModel(binary);
    AnalysisEnvironment environment = new AnalysisEnvironment(systemModel, null);
    Analysis<?> analysis =
      new CallStringAnalysis(environment, binary, AnalysisFactory.buildInitialDomain(domainHierarchy));
//    analysis.setDebugHooks(debug);
    RReilAddr startPoint = AnalysisFactory.getReconstructionStartAddress(binary);
    analysis.runFrom(startPoint);
    return analysis;
  }

  @SuppressWarnings({"unchecked", "rawtypes"}) public Analysis<?> runAnalysis (String rreilAssembly) {
    CompiledAssembler compilationUnit = CompiledAssembler.from(rreilAssembly);
    RReilPlatform platform = new RReilPlatform(compilationUnit.getDefaultSize(), compilationUnit.getInstructions());
    AnalysisEnvironment environment = new AnalysisEnvironment(platform);
    Binary binary = new RReilBinary(compilationUnit);
    Analysis<?> analysis =
      new CallStringAnalysis(environment, binary, AnalysisFactory.buildInitialDomain(domainHierarchy));
//    analysis.setDebugHooks(debug);
    RReilAddr startPoint = AnalysisFactory.getReconstructionStartAddress(binary);
    analysis.runFrom(startPoint);
    return analysis;
  }

}
