package ycomb.p9.binary.cfg.controller;

import bindead.analyses.fixpoint.CallString;

import java.awt.event.MouseEvent;

import javax.swing.Action;
import javax.swing.JPopupMenu;

import prefuse.controls.ControlAdapter;
import prefuse.visual.NodeItem;
import prefuse.visual.VisualItem;
import ycomb.p9.binary.cfg.display.CfgComponent;
import ycomb.p9.binary.cfg.display.DisassemblyComponent;
import ycomb.p9.binary.cfg.graph.AnalysisResult;
import ycomb.p9.binary.cfg.graph.CallTree;

public class CallTreeContextMenu extends ControlAdapter {

    private final AnalysisResult analysis;

    public CallTreeContextMenu(AnalysisResult analysis) {
        this.analysis = analysis;
    }

    @Override
    public void itemPressed(VisualItem item, MouseEvent e) {
        nodePopupHandler(item, e);
    }

    @Override
    public void itemReleased(VisualItem item, MouseEvent e) {
        nodePopupHandler(item, e);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        generalPopupHandler(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        generalPopupHandler(e);
    }

    private void generalPopupHandler(MouseEvent e) {
        if (!e.isPopupTrigger())
            return;
        JPopupMenu menu = showMenuForProgram();
        if (menu != null)
            menu.show(e.getComponent(), e.getX(), e.getY());
        e.consume();
    }

    private void nodePopupHandler(VisualItem item, MouseEvent e) {
        if (!e.isPopupTrigger() || !(item instanceof NodeItem))
            return;
        JPopupMenu menu;
        CallString callstring = (CallString) item.getSourceTuple().get(CallTree.$Callsite);
        if (callstring.isRoot()) {
            menu = showMenuForProgram();
        } else {
            menu = showMenuForProcedure(callstring);
        }
        if (menu != null)
            menu.show(e.getComponent(), e.getX(), e.getY());
        e.consume();
    }

    private JPopupMenu showMenuForProcedure(CallString callstring) {
        Action openRReilCfgAction = CfgComponent.getOpenRReilCfgAction(analysis, callstring);
        JPopupMenu menu = new JPopupMenu();
        menu.add(openRReilCfgAction);
        if (analysis.hasBinaryFile()) {
            Action openNativeCfgAction = CfgComponent.getOpenNativeCfgAction(analysis, callstring);
            Action openBothCfgsAction = CfgComponent.getOpenBothAction(analysis, callstring);
            menu.add(openNativeCfgAction);
            menu.add(openBothCfgsAction);
        }
        return menu;
    }

    private JPopupMenu showMenuForProgram() {
        JPopupMenu menu = new JPopupMenu();
        Action openRReilDisassemblyAction = DisassemblyComponent.getOpenRReilDisassemblyAction(analysis);
        menu.add(openRReilDisassemblyAction);
        if (analysis.hasBinaryFile()) {
            Action openNativeActionDisassemblyAction = DisassemblyComponent.getOpenNativeDisassemblyAction(analysis);
            Action openBothAction = DisassemblyComponent.getOpenBothAction(analysis);
            menu.add(openNativeActionDisassemblyAction);
            menu.add(openBothAction);
        }
        return menu;
    }
}
