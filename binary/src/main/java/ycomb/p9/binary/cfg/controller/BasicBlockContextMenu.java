package ycomb.p9.binary.cfg.controller;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.Action;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.openide.util.NbBundle.Messages;

import prefuse.controls.ControlAdapter;
import prefuse.visual.NodeItem;
import prefuse.visual.VisualItem;
import rreil.disassembler.Instruction;
import rreil.lang.RReilAddr;
import rreil.lang.Rhs.Rvar;
import ycomb.p9.binary.RReilNativeMap;
import ycomb.p9.binary.cfg.display.renderer.BasicBlockRenderer;
import ycomb.p9.binary.cfg.graph.AnalysisResult;
import ycomb.p9.binary.cfg.graph.BasicBlock;
import ycomb.p9.binary.cfg.graph.Cfg;
import ycomb.p9.binary.values.AnalysisStateDisplay;
import bindead.analyses.fixpoint.CallString;
import bindead.analyses.fixpoint.ProgramCtx;
import bindead.analyses.fixpoint.TransitionSystem;
import bindead.domainnetwork.interfaces.ProgramPoint;
import bindead.domainnetwork.interfaces.RootDomain;

import com.google.common.collect.Multimap;

@Messages({
  "# parameters: {0} is the CFG type",
  "CTL_StateShowSubMenu=Display state",
  "CTL_ShowInCfgAction=Show instruction in {0} CFG",
  "CTL_CopyToClipboardSubMenu=Copy to clipboard",
  "HINT_ShowInCfgAction=Switches to the {0} CFG view and highlights the instruction for the current address."
})
public abstract class BasicBlockContextMenu extends ControlAdapter {
  public static final String iconPath = "ycomb/p9/binary/icons/format-indent-more.png";
  protected final AnalysisResult analysis;
  protected final CallString callString;
  protected final String blockSchemaID;
  protected RReilNativeMap<ProgramCtx> calls;
  protected RReilNativeMap<ProgramCtx> returns;
  private final String functionName;

  public BasicBlockContextMenu (Cfg cfg, String functionName, String blockSchemaID) {
    analysis = cfg.getAnalysis();
    callString = cfg.getCallString();
    this.functionName = functionName;
    this.blockSchemaID = blockSchemaID;
    retrieveCalls();
    retrieveReturns();
  }

  @Override public void itemPressed (VisualItem item, MouseEvent e) {
    popupHandler(item, e);
  }

  @Override public void itemReleased (VisualItem item, MouseEvent e) {
    popupHandler(item, e);
  }

  public String getCfgName () {
    return functionName;
  }

  private void popupHandler (VisualItem item, MouseEvent e) {
    if (!e.isPopupTrigger() || !(item instanceof NodeItem))
      return;
    JPopupMenu menu = handleCfgNode((NodeItem) item);
    if (menu != null)
      menu.show(e.getComponent(), e.getX(), e.getY());
    e.consume();
  }

  protected BasicBlock getBlock (VisualItem item) {
    if (item.canGet(blockSchemaID, BasicBlock.class))
      return (BasicBlock) item.get(blockSchemaID);
    return null;
  }

  private void retrieveCalls () {
    calls = new RReilNativeMap<>();
    TransitionSystem transitionSystem = analysis.getAnalysis().getTransitionSystem();
    Multimap<ProgramPoint, ProgramCtx> callTransitions = transitionSystem.getTransitionsFor(callString).getCalls();
    if (callTransitions.isEmpty())
      return;
    for (Entry<ProgramPoint, ProgramCtx> entry : callTransitions.entries()) {
      RReilAddr sourceAddress = entry.getKey().getAddress();
      // TODO: implement this for multiple call targets.
      // Needs a multimap and a submenu for all the targets, plus some handling of too many targets!
      assert !calls.containsRReilKey(sourceAddress);
      calls.put(sourceAddress, entry.getValue());
    }
  }

  private void retrieveReturns () {
    returns = new RReilNativeMap<>();
    TransitionSystem transitionSystem = analysis.getAnalysis().getTransitionSystem();
    Multimap<ProgramPoint, ProgramCtx> returnTransitions = transitionSystem.getTransitionsFor(callString).getReturns();
    if (returnTransitions.isEmpty())
      return;
    for (Entry<ProgramPoint, ProgramCtx> entry : returnTransitions.entries()) {
      RReilAddr sourceAddress = entry.getKey().getAddress();
      // TODO: implement this for multiple call targets.
      // Needs a multimap and a submenu for all the targets, plus some handling of too many targets!
      assert !returns.containsRReilKey(sourceAddress);
      returns.put(sourceAddress, entry.getValue());
    }
  }

  /**
   * Appends a menu to the first popup menu. The first parameter is altered in-place.
   *
   * @param menu The root menu to which another menu should be appended
   * @param toBeAppended The menu to be appended
   * @param addSeparator <code>true</code> if there should be a separator line between the menus
   */
  protected static void appendMenuToMenu (JPopupMenu menu, JPopupMenu toBeAppended, boolean addSeparator) {
    if (addSeparator)
      menu.addSeparator();
    for (Component menuItem : toBeAppended.getComponents()) {
      menu.add(menuItem);
    }
  }

  protected Instruction getSelectedInstruction (NodeItem item) {
    BasicBlock instructions = getBlock(item);
    int selectedInstructionIndex = ((BasicBlockRenderer) item.getRenderer()).getSelectedLine(item);
    if (selectedInstructionIndex >= instructions.size())
      return null;
    return instructions.get(selectedInstructionIndex);
  }

  protected void addCopyToClipboardActions (NodeItem item, Instruction instruction, JPopupMenu menu) {
    JMenuItem clipboardHeader = new JMenuItem(Bundle.CTL_CopyToClipboardSubMenu());
    clipboardHeader.setEnabled(false);
    menu.add(clipboardHeader);

    appendMenuToMenu(menu, getCopyToClipboardMenu(item, instruction), false);
    menu.addSeparator();
  }

  protected void addShowDomainAction (RReilAddr selectedAddress, Set<Rvar> usedVars, JPopupMenu menu) {
    // headline
    JMenuItem header = new JMenuItem(Bundle.CTL_StateShowSubMenu());
    header.setEnabled(false);
    menu.add(header);

    RootDomain<?> domain = analysis.getAnalysis().getState(callString, selectedAddress).getOrNull();
    appendMenuToMenu(menu, getShowStateMenu(domain, selectedAddress, usedVars), false);
    menu.addSeparator();
  }

  private static JPopupMenu getCopyToClipboardMenu(VisualItem item, Instruction insn) {
    Action copyInstructionToClipboard = ClipboardActions.getCopyInstructionToClipboardAction(item, insn);
    Action copyBasicBlockToClipboard = ClipboardActions.getCopyBlockToClipboardAction(item);
    JPopupMenu clipboardMenu = new JPopupMenu(Bundle.CTL_CopyToClipboardSubMenu());
    clipboardMenu.add(copyInstructionToClipboard);
    clipboardMenu.add(copyBasicBlockToClipboard);
    return clipboardMenu;
  }

  private static JPopupMenu getShowStateMenu (RootDomain<?> state, RReilAddr address, Set<Rvar> variables) {
    JPopupMenu menu = new JPopupMenu(Bundle.CTL_StateShowSubMenu());
    Action showAllAction = AnalysisStateDisplay.getShowStateAllVariablesAction(state, address);
    Action showSomeAction = AnalysisStateDisplay.getShowStateSomeVariablesAction(state, address, variables);
    menu.add(showAllAction);
    menu.add(showSomeAction);
    if (variables.isEmpty())
      showSomeAction.setEnabled(false);
    if (state == null)
      menu.setEnabled(false);
    return menu;
  }

  protected abstract JPopupMenu handleCfgNode (NodeItem item);

}
