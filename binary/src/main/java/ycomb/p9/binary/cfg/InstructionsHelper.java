package ycomb.p9.binary.cfg;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import rreil.disassembler.Instruction;
import rreil.disassembler.OperandPostorderIterator;
import rreil.disassembler.OperandTree;
import rreil.disassembler.OperandTree.Node;
import rreil.lang.RReil;
import rreil.lang.Rhs.Rvar;
import rreil.lang.util.RvarExtractor;
import bindead.environment.platform.Platform;

/**
 * Some commonly used methods that deal with instructions.
 *
 * @author Bogdan Mihaila
 */
public class InstructionsHelper {

  /**
   * Return all the unique variables that occur in the given RREIL instruction.
   */
  public static Set<Rvar> getOccurringVariables (RReil instruction) {
    return RvarExtractor.getUnique(instruction);
  }

  /**
   * Return all the unique variables that occur in the given native instruction.
   * Uses the platform object to resolve names to registers and their sizes.
   */
  public static Set<Rvar> getOccurringVariables (Instruction nativeInstruction, Platform platform) {
    Set<Rvar> variables = new HashSet<Rvar>();
    for (String varName : getVariableNamesInNativeInstruction(nativeInstruction)) {
      Rvar variable = platform.getRegisterAsVariable(varName);
      if (variable != null)
        variables.add(variable);
    }
    return variables;
  }

  /**
   * Return all the unique variables that occur in the given native instruction.
   * It tries to match the names of variables in the RREIL instructions with names in the native instruction.
   * This might not work if the names do not match, e.g. the native register {@code ax} is translated
   * as {@code eax:16} in 32 bits RREIL.<br>
   * Unless you know what you want is this method use {@link #getOccurringVariables(Instruction, Platform)} instead.
   */
  public static Set<Rvar> getOccurringVariables (Instruction nativeInstruction,
      Collection<RReil> correspondingRReilInstructions) {
    Set<Rvar> variables = new HashSet<Rvar>();
    for (RReil insn : correspondingRReilInstructions) {
      variables.addAll(RvarExtractor.getUnique(insn));
    }
    return getVariablesOccurringInNativeInstruction(nativeInstruction, variables);
  }

  private static Set<String> getVariableNamesInNativeInstruction (Instruction insn) {
    Set<String> varNames = new HashSet<String>();
    for (OperandTree operand : insn.operands()) {
      OperandPostorderIterator it = new OperandPostorderIterator(operand.getRoot());
      while (it.next()) {
        Node current = it.current();
        if (current.getType().equals(OperandTree.Type.Sym)) {
          String name = current.getData().toString();
          varNames.add(name);
        }
      }
    }
    return varNames;
  }

  private static Set<Rvar> getVariablesOccurringInNativeInstruction (Instruction insn, Set<Rvar> allVars) {
    Set<String> usedVarsNames = getVariableNamesInNativeInstruction(insn);
    List<Rvar> usedVars = new LinkedList<Rvar>();
    for (Rvar variable : allVars) {
      String name = variable.getRegionId().toString();
      if (usedVarsNames.contains(name))
        usedVars.add(variable);
    }
    return RvarExtractor.unique(usedVars);
  }

}
