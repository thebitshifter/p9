package ycomb.p9.binary.cfg.controller.events;

public abstract class ViewEvent {
  private final String id;
  private boolean consumed;

  public ViewEvent (String id) {
    this.id = id;
    this.consumed = false;
  }

  public boolean isConsumed () {
    return consumed;
  }

  public void consume () {
    consumed = true;
  }

  public String getReceiverID () {
    return id;
  }

}
