package ycomb.p9.binary.cfg.display.renderer;

import java.awt.Font;
import java.awt.font.TextAttribute;
import java.text.AttributedString;
import java.util.Iterator;
import javalx.numeric.Interval;
import rreil.disassembler.Instruction;
import rreil.disassembler.OperandPostorderIterator;
import rreil.disassembler.OperandTree;
import rreil.disassembler.OperandTree.Node;
import rreil.lang.RReil;
import rreil.lang.RReil.Assertion;
import rreil.lang.RReil.Assign;
import rreil.lang.RReil.Branch;
import rreil.lang.RReil.BranchToNative;
import rreil.lang.RReil.BranchToRReil;
import rreil.lang.RReil.Flop;
import rreil.lang.RReil.Load;
import rreil.lang.RReil.Native;
import rreil.lang.RReil.Nop;
import rreil.lang.RReil.PrimOp;
import rreil.lang.RReil.Store;
import rreil.lang.RReil.Throw;
import rreil.lang.RReilAddr;
import rreil.lang.lowlevel.LowLevelRReilFactory;
import rreil.lang.lowlevel.LowLevelRReilOpnd;
import rreil.lang.lowlevel.RReilHighLevelToLowLevelWrapper;
import rreil.lang.util.RReilVisitor;
import ycomb.p9.binary.cfg.display.ColorConfig;
import ycomb.p9.binary.cfg.display.renderer.StringTokenizer.StringToken;
import bindead.debug.StringHelpers;

public class RReilInstructionRenderer extends BaseInstructionRenderer {
  private static final String sizeAnnotationPattern = ":\\d+(/\\d+)?";
  // match everything that starts with a letter and may contain numbers but not the strings
  // 'goto', 'native' and 'rreil'
  private static final String registersPattern = "^(?!(goto|native|rreil))[a-zA-Z]+(\\d|[a-zA-Z])*";

  @Override public boolean startsBlock (Instruction insn) {
    return insn.address().offset() == 0;
  }

  @Override public String getAddress (Instruction insn) {
    RReilAddr address = insn.address();
    String addressString = String.format("%x.%02x", address.base(), address.offset());
    return StringHelpers.padWithWitespace(0, addressString, 1);
  }

  @Override public String getOpcode (Instruction insn) {
    return "";
  }

  @Override public String getMnemonic (Instruction insn) {
    String mnemonic = insn.mnemonic();
    return StringHelpers.padWithWitespace(1, mnemonic, 1);
  }

  @Override public String getOperands (Instruction insn, boolean numbersInHex) {
    if (insn instanceof RReilHighLevelToLowLevelWrapper)
      return getOperandsRReilInstructionType(((RReilHighLevelToLowLevelWrapper) insn).toRReil(), numbersInHex);
    else
      return getOperandsLegacyInstructionType(insn, numbersInHex);
  }

  private static String getOperandsRReilInstructionType (RReil insn, @SuppressWarnings("unused") boolean numbersInHex) {
    RReilOperandsPrinter visitor = new RReilOperandsPrinter();
    return insn.accept(visitor, new StringBuilder()).toString();
  }

  private static String getOperandsLegacyInstructionType (Instruction insn, boolean numbersInHex) {
    StringBuilder builder = new StringBuilder();
    if (insn.mnemonic().equals(LowLevelRReilFactory.$Brc)) {
      LowLevelRReilOpnd cond = (LowLevelRReilOpnd) insn.operand(0);
      builder.append(cond.toString());
      LowLevelRReilOpnd target = (LowLevelRReilOpnd) insn.operand(1);
      if (target.getOffsetOrZero() != 0)
        builder.append(" goto rreil ");
      else
        builder.append(" goto native ");
      builder.append(target);
    } else {
      Iterator<OperandTree> it = insn.operands().iterator();
      while (it.hasNext()) {
        builder.append(renderOperand(it.next(), numbersInHex));
        if (it.hasNext())
          builder.append(", ");
      }
    }
    return builder.toString();
  }

  private static String renderOperand (OperandTree operand, @SuppressWarnings("unused") boolean numbersInHex) {
    StringBuilder builder = new StringBuilder();
    OperandPostorderIterator it = new OperandPostorderIterator(operand.getRoot());
    while (it.next()) {
      Node node = it.current();
      switch (node.getType()) {
      case Immf:
      case Immi:
        builder.append(((Number) node.getData()).toString());
        break;
      case Immr:
        builder.append(((Interval) node.getData()).toString());
        break;
      case Sym:
        builder.append((String) node.getData());
        break;
      case Size:
        builder.append(':').append(node.getData());
        break;
      case Op:
        builder.append((String) node.getData());
        break;
      case Mem:
        builder.append('*').append(((Number) node.getData()).toString());
        break;
      }
    }
    return builder.toString();
  }

  @Override public String getText (Instruction insn, int addressColumnSize, int opcodeColumnSize,
      int mnemonicColumnSize, boolean numbersInHex) {
    String address = getAddress(insn);
    String mnemonic = getMnemonic(insn);
    String addressSeparator = StringHelpers.repeatString(" ", addressColumnSize - address.length());
    String mnemonicSeparator = StringHelpers.repeatString(" ", mnemonicColumnSize - mnemonic.length());
    StringBuilder builder = new StringBuilder();
    builder.append(addressSeparator);
    builder.append(address);
    builder.append(mnemonic);
    builder.append(mnemonicSeparator);
    builder.append(getOperands(insn, numbersInHex));
    return builder.toString();
  }

  @Override public AttributedString styleOperands (AttributedString styledInstruction, String operandsText,
      int operandsOffsetInString, Font currentFont) {
    styleSizeAnnotation(styledInstruction, operandsText, operandsOffsetInString, currentFont);
    styleRegisters(styledInstruction, operandsText, operandsOffsetInString);
    return styledInstruction;
  }

  private static void styleRegisters (AttributedString styledInstruction, String operandsText, int operandsOffsetInString) {
    int currentOffset = operandsOffsetInString;
    String[] operands = operandsText.split("[ \\[\\]\\(\\)<>]");
    for (String operand : operands) {
      for (StringToken token : StringTokenizer.split(operand, registersPattern)) {
        if (token.isMatch) {
          int beginIndex = token.range.begin + currentOffset;
          int endIndex = token.range.end + currentOffset;
          styledInstruction.addAttribute(TextAttribute.FOREGROUND, ColorConfig.$Register, beginIndex, endIndex);
        }
      }
      currentOffset = currentOffset + operand.length() + 1;
    }
  }

  private static void styleSizeAnnotation (AttributedString styledInstruction, String operandsText,
      int operandsOffsetInString, Font currentFont) {
    for (StringToken token : StringTokenizer.split(operandsText, sizeAnnotationPattern)) {
      if (token.isMatch) {
        int beginIndex = token.range.begin + operandsOffsetInString;
        int endIndex = token.range.end + operandsOffsetInString;
        styledInstruction.addAttribute(TextAttribute.FOREGROUND, ColorConfig.$RReilSizeAnnotation, beginIndex, endIndex);
        styledInstruction.addAttribute(TextAttribute.SIZE, currentFont.getSize() - 3, beginIndex, endIndex);
      }
    }
  }


  /**
   * A dispatcher for RREIL instructions that retrieves the operands and prints them as a comma-separated list.
   *
   * @author Bogdan Mihaila
   */
  private static class RReilOperandsPrinter implements RReilVisitor<StringBuilder, StringBuilder> {

    /**
     * The default implementation just splits the mnemonic off the instruction string and returns the rest.
     * This behavior is enough for most instructions. Implement more sophisticated logic if needed.
     */
    private static StringBuilder defaultOperandsExtractor (RReil insn, StringBuilder builder) {
      String insnString = insn.toString();
      int firstSpace = insnString.indexOf(' ');
      assert firstSpace >= 0 : "A RREIL instruction should start with a non-empty mnemonic.";
      builder.append(insnString.substring(firstSpace).trim());
      return builder;
    }

    @Override public StringBuilder visit (Assign stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override public StringBuilder visit (Load stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override public StringBuilder visit (Store stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override public StringBuilder visit (BranchToNative stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override public StringBuilder visit (BranchToRReil stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override public StringBuilder visit (Nop stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override public StringBuilder visit (Assertion stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override public StringBuilder visit (Branch stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override public StringBuilder visit (PrimOp stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override public StringBuilder visit (Native stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override public StringBuilder visit (Throw stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override public StringBuilder visit (Flop stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

  }
}
