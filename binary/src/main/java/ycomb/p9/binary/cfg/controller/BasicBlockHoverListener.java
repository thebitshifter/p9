package ycomb.p9.binary.cfg.controller;

import java.awt.event.MouseEvent;

import rreil.disassembler.Instruction;
import ycomb.p9.binary.cfg.graph.BasicBlock;

/**
 * @author Raphael
 */
public interface BasicBlockHoverListener {

  public void blockEntered (BasicBlock block, MouseEvent me);

  public void blockExited (BasicBlock block, MouseEvent me);

  public void instructionEntered (BasicBlock block, Instruction instr, MouseEvent me);

  public void instructionExited (BasicBlock block, Instruction instr, MouseEvent me);
}
