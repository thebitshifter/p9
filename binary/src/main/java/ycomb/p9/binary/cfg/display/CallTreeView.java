package ycomb.p9.binary.cfg.display;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;

import prefuse.Constants;
import prefuse.Display;
import prefuse.Visualization;
import prefuse.action.Action;
import prefuse.action.ActionList;
import prefuse.action.ItemAction;
import prefuse.action.RepaintAction;
import prefuse.action.animate.ColorAnimator;
import prefuse.action.animate.LocationAnimator;
import prefuse.action.animate.QualityControlAnimator;
import prefuse.action.animate.VisibilityAnimator;
import prefuse.action.assignment.ColorAction;
import prefuse.action.assignment.FontAction;
import prefuse.action.filter.FisheyeTreeFilter;
import prefuse.action.layout.CollapsedSubtreeLayout;
import prefuse.action.layout.graph.NodeLinkTreeLayout;
import prefuse.activity.SlowInSlowOutPacer;
import prefuse.controls.Control;
import prefuse.controls.FocusControl;
import prefuse.controls.PanControl;
import prefuse.controls.WheelZoomControl;
import prefuse.controls.ZoomControl;
import prefuse.controls.ZoomToFitControl;
import prefuse.data.Node;
import prefuse.data.Tuple;
import prefuse.data.event.TupleSetListener;
import prefuse.data.search.PrefixSearchTupleSet;
import prefuse.data.tuple.TupleSet;
import prefuse.render.AbstractShapeRenderer;
import prefuse.render.DefaultRendererFactory;
import prefuse.render.EdgeRenderer;
import prefuse.render.LabelRenderer;
import prefuse.visual.VisualItem;
import prefuse.visual.expression.InGroupPredicate;
import prefuse.visual.sort.TreeDepthItemSorter;
import ycomb.p9.binary.cfg.controller.CallTreeContextMenu;
import ycomb.p9.binary.cfg.graph.CallTree;
import bindead.analyses.fixpoint.CallString;

public class CallTreeView extends Display {
  private static final long serialVersionUID = 1L;
  public static final String $Tree = "tree";
  public static final String $Nodes = "tree.nodes";
  public static final String $Edges = "tree.edges";
  public static final String $Label = CallTree.$Label;
  private final Visualization visualization = m_vis;
  private final LabelRenderer nodeRenderer;
  private final EdgeRenderer edgeRenderer;
  private final CallTree callTree;
  private int orientation = Constants.ORIENT_LEFT_RIGHT;
  private Map<CallString, Node> reverseMapping;
  private VisualItem highlightedNode;

  public CallTreeView (CallTree callTree) {
    super(new Visualization());
    this.callTree = callTree;
    visualization.add($Tree, callTree);

    nodeRenderer = new LabelRenderer($Label);
    nodeRenderer.setRenderType(AbstractShapeRenderer.RENDER_TYPE_FILL);
    nodeRenderer.setHorizontalAlignment(Constants.LEFT);
    nodeRenderer.setRoundedCorner(8, 8);
    edgeRenderer = new EdgeRenderer(Constants.EDGE_TYPE_CURVE);

    DefaultRendererFactory rf = new DefaultRendererFactory(nodeRenderer);
    rf.add(new InGroupPredicate($Edges), edgeRenderer);
    visualization.setRendererFactory(rf);

    // colors
    ItemAction nodeColor = new NodeColorAction();
    ItemAction textColor = new NodeTextColorAction();
    ItemAction edgeColor = new ColorAction($Edges, VisualItem.STROKECOLOR, ColorConfig.$Edge);

    // quick repaint
    ActionList repaint = new ActionList();
    repaint.add(textColor);
    repaint.add(nodeColor);
    repaint.add(edgeColor);
    repaint.add(new RepaintAction());
    visualization.putAction("repaint", repaint);

    // animate paint change
    ActionList animatePaint = new ActionList(400);
    animatePaint.add(new ColorAnimator($Nodes));
    animatePaint.add(new RepaintAction());
    visualization.putAction("animatePaint", animatePaint);

    // create the tree layout action
    NodeLinkTreeLayout treeLayout = new NodeLinkTreeLayout($Tree, orientation, 50, 0, 8);
    treeLayout.setLayoutAnchor(new Point2D.Double(25, 50));
    visualization.putAction("treeLayout", treeLayout);
    CollapsedSubtreeLayout subLayout = new CollapsedSubtreeLayout($Tree, orientation);
    visualization.putAction("subLayout", subLayout);

    AutoPanAction autoPan = new AutoPanAction();

    // create the filtering and layout
    ActionList filter = new ActionList();
    filter.add(new FisheyeTreeFilter($Tree, 8));
    filter.add(new FontAction($Nodes, new Font(Font.MONOSPACED, Font.PLAIN, 11)));
    filter.add(treeLayout);
    filter.add(subLayout);
    filter.add(textColor);
    filter.add(nodeColor);
    filter.add(edgeColor);
    visualization.putAction("filter", filter);

    // animated transition
    ActionList animate = new ActionList(1000);
    animate.setPacingFunction(new SlowInSlowOutPacer());
    animate.add(autoPan);
    animate.add(new QualityControlAnimator());
    animate.add(new VisibilityAnimator($Tree));
    animate.add(new LocationAnimator($Nodes));
    animate.add(new ColorAnimator($Nodes));
    animate.add(new RepaintAction());
    visualization.putAction("animate", animate);
    visualization.alwaysRunAfter("filter", "animate");

    // create animator for orientation changes
    ActionList orient = new ActionList(2000);
    orient.setPacingFunction(new SlowInSlowOutPacer());
    orient.add(autoPan);
    orient.add(new QualityControlAnimator());
    orient.add(new LocationAnimator($Nodes));
    orient.add(new RepaintAction());
    visualization.putAction("orient", orient);

    // initialize the display
    setItemSorter(new TreeDepthItemSorter());
    addControlListener(new ZoomToFitControl(Control.MIDDLE_MOUSE_BUTTON));
    addControlListener(new ZoomControl());
    addControlListener(new WheelZoomControl());
    addControlListener(new PanControl());
    addControlListener(new FocusControl(1, "filter"));
    addControlListener(new CallTreeContextMenu(callTree.getAnalysis()));

    registerKeyboardAction(
        new OrientAction(Constants.ORIENT_LEFT_RIGHT), "left-to-right", KeyStroke.getKeyStroke("ctrl shift 1"),
        WHEN_FOCUSED);
    registerKeyboardAction(
        new OrientAction(Constants.ORIENT_TOP_BOTTOM), "top-to-bottom", KeyStroke.getKeyStroke("ctrl shift 2"),
        WHEN_FOCUSED);
    registerKeyboardAction(
        new OrientAction(Constants.ORIENT_RIGHT_LEFT), "right-to-left", KeyStroke.getKeyStroke("ctrl shift 3"),
        WHEN_FOCUSED);
    registerKeyboardAction(
        new OrientAction(Constants.ORIENT_BOTTOM_TOP), "bottom-to-top", KeyStroke.getKeyStroke("ctrl shift 4"),
        WHEN_FOCUSED);

    // filter graph and perform layout
    setOrientation(orientation);
    visualization.run("filter");

    TupleSet search = new PrefixSearchTupleSet();
    visualization.addFocusGroup(Visualization.SEARCH_ITEMS, search);
    search.addTupleSetListener(new TupleSetListener() {
      @Override public void tupleSetChanged (TupleSet t, Tuple[] add, Tuple[] rem) {
        visualization.cancel("animatePaint");
        visualization.run("repaint");
        visualization.run("animatePaint");
      }
    });

    setBackground(ColorConfig.$Background);
    setForeground(ColorConfig.$Foreground);
  }

  private void initReverseMapping () {
    reverseMapping = new HashMap<CallString, Node>();
    for (Iterator<?> nodes = callTree.nodes(); nodes.hasNext();) {
      Node node = (Node) nodes.next();
      reverseMapping.put((CallString) node.get(CallTree.$Callsite), node);
    }
  }

  public void highlightFunction (CallString callString) {
    if (reverseMapping == null)
      initReverseMapping();
    Node node = reverseMapping.get(callString);
    if (node == null)
      return;
    VisualItem visualNode = visualization.getVisualItem($Nodes, node);
    if (highlightedNode != null)
      highlightedNode.setHighlighted(false);
    highlightedNode = visualNode;
    highlightedNode.setHighlighted(true);
    visualization.run("repaint");
  }

  public void setOrientation (int orientation) {
    NodeLinkTreeLayout rtl = (NodeLinkTreeLayout) visualization.getAction("treeLayout");
    CollapsedSubtreeLayout stl = (CollapsedSubtreeLayout) visualization.getAction("subLayout");
    switch (orientation) {
    case Constants.ORIENT_LEFT_RIGHT:
      nodeRenderer.setHorizontalAlignment(Constants.LEFT);
      edgeRenderer.setHorizontalAlignment1(Constants.RIGHT);
      edgeRenderer.setHorizontalAlignment2(Constants.LEFT);
      edgeRenderer.setVerticalAlignment1(Constants.CENTER);
      edgeRenderer.setVerticalAlignment2(Constants.CENTER);
      break;
    case Constants.ORIENT_RIGHT_LEFT:
      nodeRenderer.setHorizontalAlignment(Constants.RIGHT);
      edgeRenderer.setHorizontalAlignment1(Constants.LEFT);
      edgeRenderer.setHorizontalAlignment2(Constants.RIGHT);
      edgeRenderer.setVerticalAlignment1(Constants.CENTER);
      edgeRenderer.setVerticalAlignment2(Constants.CENTER);
      break;
    case Constants.ORIENT_TOP_BOTTOM:
      nodeRenderer.setHorizontalAlignment(Constants.CENTER);
      edgeRenderer.setHorizontalAlignment1(Constants.CENTER);
      edgeRenderer.setHorizontalAlignment2(Constants.CENTER);
      edgeRenderer.setVerticalAlignment1(Constants.BOTTOM);
      edgeRenderer.setVerticalAlignment2(Constants.TOP);
      break;
    case Constants.ORIENT_BOTTOM_TOP:
      nodeRenderer.setHorizontalAlignment(Constants.CENTER);
      edgeRenderer.setHorizontalAlignment1(Constants.CENTER);
      edgeRenderer.setHorizontalAlignment2(Constants.CENTER);
      edgeRenderer.setVerticalAlignment1(Constants.TOP);
      edgeRenderer.setVerticalAlignment2(Constants.BOTTOM);
      break;
    default:
      throw new IllegalArgumentException("Unrecognized orientation value: " + orientation);
    }
    this.orientation = orientation;
    rtl.setOrientation(orientation);
    stl.setOrientation(orientation);
  }

  public int getOrientation () {
    return orientation;
  }

  public class OrientAction extends AbstractAction {
    private final int orientation;

    public OrientAction (int orientation) {
      this.orientation = orientation;
    }

    @Override public void actionPerformed (ActionEvent evt) {
      setOrientation(orientation);
      getVisualization().cancel("orient");
      getVisualization().run("treeLayout");
      getVisualization().run("orient");
    }
  }

  public class AutoPanAction extends Action {
    private final Point2D m_start = new Point2D.Double();
    private final Point2D m_end = new Point2D.Double();
    private final Point2D m_cur = new Point2D.Double();
    private final int m_bias = 150;

    @Override public void run (double frac) {
      TupleSet ts = m_vis.getFocusGroup(Visualization.FOCUS_ITEMS);
      if (ts.getTupleCount() == 0)
        return;

      if (frac == 0.0) {
        int xbias = 0, ybias = 0;
        switch (orientation) {
        case Constants.ORIENT_LEFT_RIGHT:
          xbias = m_bias;
          break;
        case Constants.ORIENT_RIGHT_LEFT:
          xbias = -m_bias;
          break;
        case Constants.ORIENT_TOP_BOTTOM:
          ybias = m_bias;
          break;
        case Constants.ORIENT_BOTTOM_TOP:
          ybias = -m_bias;
          break;
        }

        VisualItem vi = (VisualItem) ts.tuples().next();
        m_cur.setLocation(getWidth() / 2, getHeight() / 2);
        getAbsoluteCoordinate(m_cur, m_start);
        m_end.setLocation(vi.getX() + xbias, vi.getY() + ybias);
      } else {
        m_cur.setLocation(m_start.getX() + frac * (m_end.getX() - m_start.getX()),
            m_start.getY() + frac * (m_end.getY() - m_start.getY()));
        panToAbs(m_cur);
      }
    }
  }

  public static class NodeColorAction extends ColorAction {

    public NodeColorAction () {
      super($Nodes, VisualItem.FILLCOLOR);
    }

    @Override public int getColor (VisualItem item) {
      if (item.isHighlighted())
        return ColorConfig.$FunctionHighlight;
      if (m_vis.isInGroup(item, Visualization.SEARCH_ITEMS))
        return ColorConfig.$SearchBg;
      if (m_vis.isInGroup(item, Visualization.FOCUS_ITEMS))
        return ColorConfig.$FocusBg;
      if (item.getDOI() > -1)
        return ColorConfig.$DOIBg;
      return ColorConfig.$NormalBg;
    }
  }

  public static class NodeTextColorAction extends ColorAction {

    public NodeTextColorAction () {
      super($Nodes, VisualItem.TEXTCOLOR);
    }

    @Override public int getColor (VisualItem item) {
      if (item.isHighlighted())
        return ColorConfig.$FocusFg;
      if (m_vis.isInGroup(item, Visualization.SEARCH_ITEMS))
        return ColorConfig.$SearchFg;
      if (m_vis.isInGroup(item, Visualization.FOCUS_ITEMS))
        return ColorConfig.$FocusFg;
      if (item.getDOI() > -1)
        return ColorConfig.$DOIFg;
      return ColorConfig.$NormalFg;
    }
  }
}
