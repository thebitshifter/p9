package ycomb.p9.binary.cfg.display.renderer;

import java.awt.Font;
import java.awt.font.TextAttribute;
import java.text.AttributedString;

import rreil.disassembler.Instruction;
import ycomb.p9.binary.cfg.display.ColorConfig;
import ycomb.p9.binary.cfg.display.renderer.StringTokenizer.StringToken;
import bindead.debug.StringHelpers;
import binspot.x86.common.X86PrettyPrinter;


public class X86InstructionRenderer extends BaseInstructionRenderer {
  // x86 registers
  private static final String registersPattern =
    "[er]?[abcd][xhl]|[er]?[sb]pl?|[er]?[sd]il?|r\\d{1,2}[bwd]?|[er]?ip|[cdefgs]s";

  @Override public boolean startsBlock (Instruction insn) {
    return false;
  }

  @Override public String getText (Instruction insn, int addressColumnSize, int opcodeColumnSize,
      int mnemonicColumnSize, boolean numbersInHex) {
    String address = getAddress(insn);
    String opcode = getOpcode(insn);
    String mnemonic = getMnemonic(insn);
    String addressSeparator = StringHelpers.repeatString(" ", addressColumnSize - address.length());
    String opcodeSeparator = StringHelpers.repeatString(" ", opcodeColumnSize - opcode.length());
    String mnemonicSeparator = StringHelpers.repeatString(" ", mnemonicColumnSize - mnemonic.length());
    StringBuilder builder = new StringBuilder();
    builder.append(addressSeparator);
    builder.append(address);
    builder.append(opcode);
    builder.append(opcodeSeparator);
    builder.append(mnemonic);
    builder.append(mnemonicSeparator);
    builder.append(getOperands(insn, numbersInHex));
    return builder.toString();
  }
  
  @Override public String getAddress (Instruction insn) {
    return StringHelpers.padWithWitespace(0, Long.toHexString(insn.baseAddress()), 1);
  }

  @Override public String getOpcode (Instruction insn) {
    StringBuilder builder = new StringBuilder();
    insn.opcode(builder);
    StringHelpers.padWithWitespace(1, builder, 1);
    return builder.toString();
  }

  @Override public String getMnemonic (Instruction insn) {
    return StringHelpers.padWithWitespace(1, insn.mnemonic(), 1);
  }

  @Override public String getOperands (Instruction insn, boolean numbersInHex) {
    X86PrettyPrinter formatter = new X86PrettyPrinter(insn);
    return formatter.getOperands(numbersInHex);
  }

  @Override public AttributedString styleOperands (AttributedString styledInstruction, String operandsText,
      int operandsOffsetInString, Font currentFont) {
    styleRegisters(styledInstruction, operandsText, operandsOffsetInString);
    return styledInstruction;
  }

  private static void styleRegisters (final AttributedString styledInstruction, String operandsText,
      final int operandsOffsetInString) {
    for (StringToken token : StringTokenizer.split(operandsText, registersPattern)) {
      if (token.isMatch) {
        int beginIndex = token.range.begin + operandsOffsetInString;
        int endIndex = token.range.end + operandsOffsetInString;
        styledInstruction.addAttribute(TextAttribute.FOREGROUND, ColorConfig.$Register, beginIndex, endIndex);
      }
    }
  }

}
