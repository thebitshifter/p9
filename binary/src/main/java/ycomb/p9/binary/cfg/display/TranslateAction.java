package ycomb.p9.binary.cfg.display;

import prefuse.Display;
import prefuse.action.Action;

/**
 * Translates a display by a given amount.
 */
public class TranslateAction extends Action {
  private final int dx;
  private final int dy;
  private final Display display;

  public TranslateAction (int dx, int dy, Display display) {
    this.dx = dx;
    this.dy = dy;
    this.display = display;
  }

  @Override public void run (double frac) {
    display.pan(dx, dy);
    display.repaint();
  }

}
