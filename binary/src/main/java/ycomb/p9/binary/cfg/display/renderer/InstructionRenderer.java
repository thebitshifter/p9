package ycomb.p9.binary.cfg.display.renderer;

import java.awt.Font;
import java.text.AttributedString;

import rreil.disassembler.Instruction;

/**
 * An instructions renderer knows how to format an instruction to a string.
 */
public interface InstructionRenderer {

  public String getAddress (Instruction insn);

  public String getOpcode (Instruction insn);

  public String getMnemonic (Instruction insn);

  public String getOperands (Instruction insn, boolean numbersInHexs);

  /**
   * Some instructions are grouped together (e.g. RREIL instructions that are one native instruction). This should
   * indicate if the instruction is the beginning of such a group.
   */
  public boolean startsBlock (Instruction insn);

  /**
   * Get the formatted text representation of the instruction.
   *
   * @param insn The instruction to be formatted
   * @param addressColumnSize If more instructions are set in a block then this hint will help to align them if a
   *          monospace font is used
   * @param opcodeColumnSize If more instructions are set in a block then this hint will help to align them if a
   *          monospace font is used
   * @param mnemonicColumnSize If more instructions are set in a block then this hint will help to align them if a
   *          monospace font is used
   * @param numbersInHex If the immediate values should be printed in hexadecimal
   * @return The string representation of the instruction
   */
  public String getText (Instruction insn, int addressColumnSize, int opcodeColumnSize, int mnemonicColumnSize,
      boolean numbersInHex);

  /**
   * Add style attributes to syntax highlight the operands of an instruction.
   *
   * @param styledInstruction The whole instruction string as styled text. Use this to set the style for the operands
   * @param operandsText The plain text for the operands. Use this to find the positions of certain operand types
   * @param operandsOffsetInString The offset of the operands string in the whole instruction string. Needed to know the
   *          offsets when applying style attributes to the whole string
   * @param currentFont The font used for the while instruction string. Use this to build derived fonts for the operands.
   * @return A styled string for the whole instruction with special style settings for the operands
   */
  public AttributedString styleOperands (AttributedString styledInstruction, String operandsText,
      int operandsOffsetInString, Font currentFont);

}
