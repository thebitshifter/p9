package ycomb.p9.binary.cfg.display.renderer;

/**
 * A tuple of begin and end indices into a string to denote a substring.
 *
 * @author Bogdan Mihaila
 */
public class TextRange {
  public final int begin;
  public final int end;

  public TextRange (int begin, int end) {
    assert begin <= end;
    this.begin = begin;
    this.end = end;
  }

  public static TextRange at (int begin, int end) {
    return new TextRange(begin, end);
  }

  @Override public String toString () {
    return "[" + begin + "-" + end + "]";
  }
}
