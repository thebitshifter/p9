package ycomb.p9.binary.cfg.graph;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import prefuse.data.Node;
import prefuse.data.Schema;
import prefuse.data.Tree;
import bindead.analyses.fixpoint.CallString;
import bindead.analyses.fixpoint.ProgramCtx;
import bindead.analyses.fixpoint.TransitionSystem;
import bindead.analyses.fixpoint.TransitionSystem.ProceduralTransitions;

/**
 * A tree of all the calls in a program.
 * Note that this is not a call-graph as each path is distinctive depending on its call-string.
 *
 * @author Bogdan Mihaila
 */
public class CallTree extends Tree {
  public static final String $Label = "label";
  public static final String $Callsite = "callsite";
  private static final Schema $Schema = new Schema();
  private final AnalysisResult analysis;
  private final TransitionSystem transitionSystem;
  private Map<CallString, Node> nodes;

  static {
    $Schema.addColumn($Label, String.class, "");
    $Schema.addColumn($Callsite, CallString.class);
  }

  public CallTree (AnalysisResult analysis) {
    this.analysis = analysis;
    this.transitionSystem = analysis.getAnalysis().getTransitionSystem();
    getNodeTable().addColumns($Schema);
    buildCallTree();
  }

  public AnalysisResult getAnalysis () {
    return analysis;
  }

  private void buildCallTree () {
    nodes = new HashMap<>();
    Map<CallString, ProceduralTransitions> procedures = transitionSystem.getAllProcedures();
    if (procedures.isEmpty())
      procedures.put(CallString.root(), new ProceduralTransitions());
    for (Entry<CallString, ProceduralTransitions> entry : procedures.entrySet()) {
      Node source = getOrFresh(entry.getKey());
      for (ProgramCtx targetPoint : entry.getValue().getCalls().values()) {
        Node target = getOrFresh(targetPoint.getCallString());
        addEdge(source, target);
      }
    }
  }

  private Node getOrFresh (CallString key) {
    Node node = nodes.get(key);
    if (node != null)
      return node;
    node = addNode();
    node.set($Label, Cfg.getName(analysis, key));
    node.set($Callsite, key);
    nodes.put(key, node);
    return node;
  }

}
