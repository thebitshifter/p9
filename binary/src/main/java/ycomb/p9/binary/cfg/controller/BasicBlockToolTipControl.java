package ycomb.p9.binary.cfg.controller;

import java.awt.event.MouseEvent;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;

import javalx.data.Option;
import javalx.numeric.Interval;
import javalx.numeric.Range;

import javax.swing.ToolTipManager;

import prefuse.Display;
import prefuse.controls.ControlAdapter;
import rreil.disassembler.Instruction;
import rreil.lang.RReilAddr;
import rreil.lang.Rhs.Rvar;
import rreil.lang.lowlevel.LowLevelRReil;
import ycomb.p9.binary.cfg.InstructionsHelper;
import ycomb.p9.binary.cfg.graph.AnalysisResult;
import ycomb.p9.binary.cfg.graph.BasicBlock;
import ycomb.p9.binary.cfg.graph.Cfg;
import bindead.analyses.fixpoint.CallString;
import bindead.debug.DomainQueryHelpers;
import bindead.domainnetwork.channels.SetOfEquations;
import bindead.domainnetwork.interfaces.RootDomain;
import bindead.domains.pointsto.PointsToSet;

import com.googlecode.jatl.Html;

/**
 * Shows tooltips with the most important values for the variables in an instruction.
 *
 * @author Raphael
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class BasicBlockToolTipControl extends ControlAdapter implements BasicBlockHoverListener {
  private final CallString callString;
  private final AnalysisResult analysis;
  private final int defaultTooltipsInitialDelay = ToolTipManager.sharedInstance().getInitialDelay();
  private final int defaultTooltipsDismissDelay = ToolTipManager.sharedInstance().getDismissDelay();

  public BasicBlockToolTipControl (Cfg cfg) {
    this.callString = cfg.getCallString();
    this.analysis = cfg.getAnalysis();
  }

  // event handling methods

  @Override public void blockEntered (BasicBlock block, MouseEvent e) {
    // no tooltips for BasicBlocks so far...
  }

  @Override public void blockExited (BasicBlock block, MouseEvent e) {
    // no tooltips for BasicBlocks so far...
  }

  @Override public void instructionEntered (BasicBlock block, Instruction instr, MouseEvent e) {
    RReilAddr selectedAddress = instr.address();
    // prepare states
    // state before instruction:
    Option<RootDomain> stateBefore =
      Option.fromNullable((RootDomain) analysis.getAnalysis().getState(callString, selectedAddress).getOrNull());
    // state after instruction:
    Option<RootDomain> stateAfter = Option.none();

    // TODO: improve this by passing both the current insn and its successor using the selected line
    // and the basic block in the caller code.
    Iterator<Instruction> iter = block.iterator();
    // try to find #instr in the #block and obtain the instruction directly after #instr:
    while (iter.hasNext()) {
      if (iter.next() == instr) {
        try {
          RReilAddr nextInstructionAddress = iter.next().address();
          stateAfter =
            Option.fromNullable((RootDomain) analysis.getAnalysis().getState(callString, nextInstructionAddress)
                .getOrNull());
        } catch (NoSuchElementException exc) {
          // Never mind! It seems that instr was just the last instruction from the block!
        }
        break;
      }
    }

    Set<Rvar> variables;
    if (instr instanceof LowLevelRReil)
      variables = InstructionsHelper.getOccurringVariables(((LowLevelRReil) instr).toRReil());
    else
      variables = InstructionsHelper.getOccurringVariables(instr, analysis.getAnalysis().getPlatform());

    if (!variables.isEmpty()) {
      // show tooltips later and forever
      ToolTipManager.sharedInstance().setInitialDelay(500);
      ToolTipManager.sharedInstance().setDismissDelay(Integer.MAX_VALUE);
      Display display = (Display) e.getSource();
      // build the tooltip
      display.setToolTipText(statesToHTML(selectedAddress, stateBefore, stateAfter, variables));
    }
  }

  @Override public void instructionExited (BasicBlock block, Instruction instr, MouseEvent me) {
    // reset the tooltips delays
    ToolTipManager.sharedInstance().setInitialDelay(defaultTooltipsInitialDelay);
    ToolTipManager.sharedInstance().setDismissDelay(defaultTooltipsDismissDelay);
    Display display = (Display) me.getSource();
    display.setToolTipText(null);
  }

  // procedures for typesetting the tooltip

  private static String statesToHTML (
      final RReilAddr address,
      final Option<RootDomain> stateBefore,
      final Option<RootDomain> stateAfter,
      final Set<Rvar> variables) {

    final StringWriter htmlStringWriter = new StringWriter();
    new Html(htmlStringWriter) {
      {
        indent(indentOff); // needed as Swing Tooltips HTML seem to choke on tab indented HTML strings
        html();
        body();
        // headline (showing the address)
        p().style("padding-bottom: 3px;");
        i().text(address.toStringWithHexPrefix() + ":").end();
        end(); // p
        // state before
        u().text("Before").end();
        variableSetToHTML(stateBefore, variables, htmlStringWriter);
        br();
        // state after
        u().text("After").end();
        variableSetToHTML(stateAfter, variables, htmlStringWriter);
        end(); // body
        end(); // html
        done();
      }
    };
    return htmlStringWriter.toString();
  }

  private static Html variableSetToHTML (final Option<RootDomain> state, final Set<Rvar> variables, Writer writer) {
    final String spacer = "&nbsp;&nbsp;&nbsp;";
    return new Html(writer) {
      {
        if (state.isNone()) {
          br();
          text("--");
        } else {
          for (Rvar variable : variables) {
            br();
            StringBuilder builder = new StringBuilder();
            b().text(variable.getRegionId().toString()).end();
            String sizeAnnotation = ":" + variable.getSize();
            if (variable.getOffset() != 0)
              sizeAnnotation = sizeAnnotation + "/" + variable.getOffset();
            span().style("font-size: 70%;").text(sizeAnnotation).end();
            text(" = ");
            Range range = DomainQueryHelpers.queryRange(variable, state.get());
            if (range == null) {
              text("<unknown>");
            } else {
              Interval value = range.convexHull();
              text(value.toString());
            }
            PointsToSet pointsTo = DomainQueryHelpers.queryPointsToSet(variable, state.get());
            if (pointsTo != null && !pointsTo.isEmpty()) {
              b().raw(spacer).text("  pts: ").end();
              text(pointsTo.toString());
            }
            SetOfEquations equalities = DomainQueryHelpers.queryEqualities(variable, state.get());
            if (equalities != null && !equalities.isEmpty()) {
              b().raw(spacer).text("  eqs: ").end();
              text(DomainQueryHelpers.formatLinearEqualities(equalities));
            }
            text(builder.toString());
          }
        }
        br();
        done();
      }
    };
  }


}
