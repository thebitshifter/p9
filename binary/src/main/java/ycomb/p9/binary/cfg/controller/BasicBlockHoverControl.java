package ycomb.p9.binary.cfg.controller;

import java.awt.event.MouseEvent;

import prefuse.controls.ControlAdapter;
import prefuse.visual.NodeItem;
import prefuse.visual.VisualItem;
import rreil.disassembler.Instruction;
import ycomb.p9.binary.cfg.display.renderer.BasicBlockRenderer;
import ycomb.p9.binary.cfg.graph.BasicBlock;

/**
 * @author Raphael Dümig
 */
public class BasicBlockHoverControl extends ControlAdapter {
  private final String blockSchemaID;
  private VisualItem currentItem;
  private BasicBlock currentBlock;
  private int currentLine;

  protected BasicBlockHoverListener listener;

  public BasicBlockHoverControl (String blockSchemaID) {
    this(null, blockSchemaID);
  }

  public BasicBlockHoverControl (BasicBlockHoverListener listener, String blockSchemaID) {
    this.listener = listener;
    this.currentBlock = null;
    this.blockSchemaID = blockSchemaID;
    this.currentLine = -1;
  }

  private void updateBlock (VisualItem item) {
    if (item.canGet(blockSchemaID, BasicBlock.class))
      currentBlock = (BasicBlock) item.get(blockSchemaID);
    else
      currentBlock = null;
  }


  @Override public void itemEntered (VisualItem item, MouseEvent e) {
    if (!(item instanceof NodeItem))
      return;
    currentItem = item;
    updateBlock(item);
    blockEntered(currentBlock, e);
  }

  @Override public void itemExited (VisualItem item, MouseEvent e) {
    if (!(item instanceof NodeItem))
      return;
    assert (item == currentItem);
    blockExited(currentBlock, e);
    currentItem = null;
    currentBlock = null;
    currentLine = -1;
  }

  @Override public void itemMoved (VisualItem item, MouseEvent e) {
    if (!(item instanceof NodeItem))
      return;
    assert (item == currentItem);
    int line = ((BasicBlockRenderer) currentItem.getRenderer()).getSelectedLine(currentItem);

    if (line != currentLine) {
      if (currentLine != -1)
        instructionExited(currentBlock, currentBlock.get(currentLine), e);
      try {
        if (line != -1)
          instructionEntered(currentBlock, currentBlock.get(line), e);
        // update the current line
        currentLine = line;
      } catch (IndexOutOfBoundsException exc) {
        currentLine = -1;
      }
    }
  }

  public BasicBlock getHoveredBlock () {
    return currentBlock;
  }

  public Instruction getHoveredInstruction () {
    return currentBlock.get(currentLine);
  }

  public void blockEntered (BasicBlock block, MouseEvent me) {
    listener.blockEntered(block, me);
  }

  public void blockExited (BasicBlock block, MouseEvent me) {
    // check if we already left the instruction
    if (currentLine != -1) {
      // if not, send the event
      instructionExited(currentBlock, currentBlock.get(currentLine), me);
      currentLine = -1;
    }
    listener.blockExited(block, me);
  }

  public void instructionEntered (BasicBlock block, Instruction instr, MouseEvent me) {
    listener.instructionEntered(block, instr, me);
  }

  public void instructionExited (BasicBlock block, Instruction instr, MouseEvent me) {
    listener.instructionExited(block, instr, me);
  }

}
