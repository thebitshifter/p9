package ycomb.p9.binary.cfg.display;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLayeredPane;

import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;

import rreil.disassembler.Instruction;
import rreil.lang.RReilAddr;
import ycomb.p9.binary.WindowsUtils;
import ycomb.p9.binary.cfg.controller.NativeContextMenu;
import ycomb.p9.binary.cfg.controller.events.CallTreeViewEventBus;
import ycomb.p9.binary.cfg.controller.events.CallTreeViewEventBus.CallTreeFocusContextEvent;
import ycomb.p9.binary.cfg.controller.events.CfgViewEventBus;
import ycomb.p9.binary.cfg.controller.events.CfgViewEventBus.CfgFocusAddressEvent;
import ycomb.p9.binary.cfg.controller.events.CfgViewEventBus.CfgViewEventListener;
import ycomb.p9.binary.cfg.display.InstructionsSearcher.InstructionTextFormatter;
import ycomb.p9.binary.cfg.display.renderer.InstructionRenderer;
import ycomb.p9.binary.cfg.display.renderer.X86InstructionRenderer;
import ycomb.p9.binary.cfg.graph.AnalysisResult;
import ycomb.p9.binary.cfg.graph.BasicBlockTransformation;
import ycomb.p9.binary.cfg.graph.Cfg;
import ycomb.p9.binary.cfg.graph.CfgBuilder;
import bindead.analyses.fixpoint.CallString;
import javax.swing.JButton;
import javax.swing.JToolBar;
import ycomb.p9.binary.cfg.controller.RReilContextMenu;
import ycomb.p9.binary.cfg.display.renderer.RReilInstructionRenderer;

@TopComponent.Description(preferredID = "CfgComponent", 
        iconBase = CfgComponent.iconPath,
        persistenceType = TopComponent.PERSISTENCE_NEVER)
@TopComponent.Registration(mode = "cfg", openAtStartup = false)
@Messages({
  "# parameters: {0} is the CFG type; {1} filler verbs/adjectives/cfg description or empty",
  "CTL_CfgComponentAction=Open {1}CFG ({0})",
  "CTL_CfgComponentBothAction=Open both CFGs ({0} and RREIL)",
  "HINT_CfgComponentAction=Display the {0} Control Flow Graph for the {1}function.",
  "HINT_CfgComponentBothAction=Display the {0} and RREIL Control Flow Graphs for the function.",
  "# parameters: {0} the CFG type; {1} the function name; {2} the file name; {3} the corresponding call string",
  "CTL_CfgComponentTitle=CFG ({0}): {1}",
  "HINT_CfgComponent={0} Control Flow Graph from file: {1} for the function: {2} with call string: {3}."
})
public class CfgComponent extends TopComponent {

  private static final long serialVersionUID = 1L;
  public static final String iconPath = "ycomb/p9/binary/icons/code-class.png";
  private static final ImageIcon imageIcon = ImageUtilities.loadImageIcon(iconPath, false);
  private final JComponent searchPanel;
  private final JComponent overviewPanel;
  private final CfgView cfgView;
  private CfgViewEventListener listener;
  private final JToolBar toolbar;

  private CfgComponent(CfgView view, String eventsID) {
    this.cfgView = view;
    searchPanel = initSearchPanel(view, view.getInstructionRenderer());
    overviewPanel = initOverviewPanel(view);
    JComponent body = new JLayeredPane();
    body.add(view, Integer.valueOf(0));
    body.add(overviewPanel, Integer.valueOf(1));
    body.add(searchPanel, Integer.valueOf(2));
    body.setBackground(ColorConfig.$Background);
    setLayout(new BorderLayout());
    toolbar = initToolbar(view);
    add(toolbar, BorderLayout.NORTH);
    add(body, BorderLayout.CENTER);
    setBackground(ColorConfig.$Background);
    setForeground(ColorConfig.$Foreground);
    WindowsUtils.dockInMode("cfg", this);
    registerEventsListener(eventsID);
  }

  @Override
  protected void componentClosed() {
    CfgViewEventBus.getInstance().removeListener(listener);
  }

  @Override
  protected void componentActivated() {
    super.componentActivated();
    Cfg cfg = cfgView.getCfg();
    CallTreeViewEventBus.getInstance().publish(new CallTreeFocusContextEvent(cfg.getAnalysis(), cfg.getCallString()));
  }

  private void registerEventsListener(final String interestedId) {
    listener = new CfgViewEventListener() {
      @Override
      public void notify(CfgFocusAddressEvent event) {
        if (!event.isConsumed() && event.getReceiverID().equals(interestedId)) {
          goToAddress(event.getAddress());
          event.consume();
        }
      }
    };
    CfgViewEventBus.getInstance().addListener(listener);
  }

  private void goToAddress(RReilAddr address) {
    requestVisible();
    cfgView.panToInstruction(address);
  }

  @Override
  public void doLayout() {
    super.doLayout();
    // need to set the bounds explicitly as the layered pane does not have a layout manager
    Rectangle bounds = getBounds();
    cfgView.setBounds(bounds);
    Dimension searchPanelSize = searchPanel.getSize();
    searchPanel.setBounds(0, bounds.height - searchPanelSize.height, bounds.width, searchPanelSize.height);
    Dimension overviewPanelSize = overviewPanel.getSize();
    int insetsX = 6;
    int insetsY = 6;
    overviewPanel.setBounds(insetsX, bounds.height - overviewPanelSize.height - insetsY, bounds.width, overviewPanelSize.height);
  }

  public static CfgComponent buildNativeCfg(AnalysisResult analysis, CallString callstring) {
    String functionName = Cfg.getName(analysis, callstring);
    String architecture = analysis.getAnalysis().getBinaryCode().getBinary().getArchitectureName();
    // FIXME: get the renderer from a map that depends on the architecture
    X86InstructionRenderer instructionRenderer = new X86InstructionRenderer();
    Cfg function = BasicBlockTransformation.run(new CfgBuilder(analysis, callstring).nativeCfg());
    CfgView view = new CfgView(function, instructionRenderer);
    view.addControlListener(new NativeContextMenu(function, functionName, Cfg.$Block));
    String fileName = analysis.getAnalysis().getBinaryCode().getBinary().getFileName();
    CfgComponent nativeCfgDisplay = new CfgComponent(view, CfgViewEventBus.getNativeEventsID(analysis, callstring));
    nativeCfgDisplay.setDisplayName(Bundle.CTL_CfgComponentTitle(architecture, functionName));
    nativeCfgDisplay.setToolTipText(Bundle.HINT_CfgComponent(architecture, fileName, functionName, callstring));
    return nativeCfgDisplay;
  }

  public static void showNativeCfg(AnalysisResult analysis, CallString callstring) {
    CfgComponent nativeCfgDisplay = buildNativeCfg(analysis, callstring);
    nativeCfgDisplay.open();
    nativeCfgDisplay.requestActive();
  }

  public static CfgComponent buildRReilCfg(AnalysisResult analysis, CallString callstring) {
    String functionName = Cfg.getName(analysis, callstring);
    RReilInstructionRenderer instructionRenderer = new RReilInstructionRenderer();
    instructionRenderer.disableOpcode(true);
    Cfg function = BasicBlockTransformation.run(new CfgBuilder(analysis, callstring).rreilCfg());
    CfgView view = new CfgView(function, instructionRenderer);
    view.addControlListener(new RReilContextMenu(function, functionName, Cfg.$Block));
    CfgComponent rreilCfgDisplay = new CfgComponent(view, CfgViewEventBus.getRReilEventsID(analysis, callstring));
    String fileName = analysis.getAnalyzedFileName();
    String architecture = "RREIL";
    rreilCfgDisplay.setDisplayName(Bundle.CTL_CfgComponentTitle(architecture, functionName));
    rreilCfgDisplay.setToolTipText(Bundle.HINT_CfgComponent(architecture, fileName, functionName, callstring));
    return rreilCfgDisplay;
  }

  public static void showRReilCfg(AnalysisResult analysis, CallString callstring) {
    CfgComponent rreilCfgDisplay = buildRReilCfg(analysis, callstring);
    rreilCfgDisplay.open();
    rreilCfgDisplay.requestActive();
  }

  public static Action getOpenRReilCfgAction(final AnalysisResult analysis, final CallString callstring) {
    Action action = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        showRReilCfg(analysis, callstring);
      }
    };
    String architecture = "RREIL";
    action.putValue(Action.SMALL_ICON, imageIcon);
    action.putValue(Action.NAME, Bundle.CTL_CfgComponentAction(architecture, ""));
    action.putValue(Action.SHORT_DESCRIPTION, Bundle.HINT_CfgComponentAction(architecture, ""));
    return action;
  }

  public static Action getOpenNativeCfgAction(final AnalysisResult analysis, final CallString callstring) {
    Action action = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        showNativeCfg(analysis, callstring);
      }
    };
    String architecture = analysis.getAnalysis().getBinaryCode().getBinary().getArchitectureName();
    action.putValue(Action.SMALL_ICON, imageIcon);
    action.putValue(Action.NAME, Bundle.CTL_CfgComponentAction(architecture, ""));
    action.putValue(Action.SHORT_DESCRIPTION, Bundle.HINT_CfgComponentAction(architecture, ""));
    return action;
  }

  public static Action getOpenBothAction(final AnalysisResult analysis, final CallString callstring) {
    Action action = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        showNativeCfg(analysis, callstring);
        RunAfter.delay(700, new Runnable() { // a workaround to open the displays with some delay
          @Override
          public void run() {
            showRReilCfg(analysis, callstring);
          }
        });
      }
    };
    String architecture = analysis.getAnalysis().getBinaryCode().getBinary().getArchitectureName();
    action.putValue(Action.SMALL_ICON, imageIcon);
    action.putValue(Action.NAME, Bundle.CTL_CfgComponentBothAction(architecture));
    action.putValue(Action.SHORT_DESCRIPTION, Bundle.HINT_CfgComponentBothAction(architecture));
    return action;
  }

  public static void setCfgOpenActionDescription(Action action, String architecture, String description) {
    action.putValue(Action.SMALL_ICON, imageIcon);
    action.putValue(Action.NAME, Bundle.CTL_CfgComponentAction(architecture, description));
    action.putValue(Action.SHORT_DESCRIPTION, Bundle.HINT_CfgComponentAction(architecture, description));
  }

  private static JToolBar initToolbar(CfgView view) {
    JToolBar toolbar = new JToolBar("CFG actions");
    toolbar.add(new JButton(getRelayoutAction(view)));
    toolbar.setRollover(true);
    return toolbar;
  }

  // TODO: make this a context aware netbeans actions in an own class. Then it can be also added to the global menu.
  @Messages({
    "CTL_RelayoutCfgAction=Relayout",
    "HINT_RelayoutCfgAction=Relayout the CFG",})
  private static Action getRelayoutAction(final CfgView view) {
    Action action = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        view.relayout();
      }
    };
    ImageIcon smallImage = ImageUtilities.loadImageIcon("ycomb/p9/binary/icons/distribute-vertical-bottom-16x16.png", false);
    action.putValue(Action.SMALL_ICON, smallImage);
    action.putValue(Action.NAME, Bundle.CTL_RelayoutCfgAction());
    action.putValue(Action.SHORT_DESCRIPTION, Bundle.HINT_RelayoutCfgAction());
    return action;
  }

  private static JComponent initOverviewPanel(CfgView view) {
    Overview overviewPanel = new Overview(view);
    final Dimension overviewPanelSize = new Dimension(150, 150);
    overviewPanel.setSize(overviewPanelSize);
    overviewPanel.setMaximumSize(overviewPanelSize);
    overviewPanel.setPreferredSize(overviewPanelSize);
    final Box overviewStripe = new Box(BoxLayout.X_AXIS);
    overviewStripe.add(overviewPanel);
    overviewStripe.add(Box.createHorizontalGlue());
    // adding this is a workaround, otherwise the overview panel would display in only half of its height
    overviewStripe.add(Box.createVerticalStrut(overviewPanelSize.height));
    // setting the bounds initially to something is a workaround otherwise the height won't stay as set above
    overviewStripe.setBounds(0, 0, overviewPanelSize.width, overviewPanelSize.height);
    return overviewStripe;
  }

  private static JComponent initSearchPanel(CfgView view, final InstructionRenderer insnRenderer) {
    Box searchStripe = new Box(BoxLayout.X_AXIS);
    searchStripe.add(Box.createHorizontalGlue());
    InstructionTextFormatter formatter = new InstructionTextFormatter() {
      @Override
      public Object format(final Instruction insn) {
        return new Object() {
          @Override
          public String toString() {
            return insnRenderer.getText(insn, 0, 0, 0, true);
          }
        };
      }
    };
    InstructionsSearcher searcher = new InstructionsSearcher(view.getCfg(), view, formatter);
    JComponent searchBox = searcher.buildSearchPanel();
    searchStripe.add(searchBox);
    Dimension searchPanelSize = searchStripe.getPreferredSize();
    searchStripe.setSize(searchPanelSize);
    searchStripe.setMaximumSize(searchPanelSize);
    searchStripe.setPreferredSize(searchPanelSize);
    return searchStripe;
  }

}
