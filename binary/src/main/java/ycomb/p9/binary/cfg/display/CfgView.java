package ycomb.p9.binary.cfg.display;

import java.awt.BorderLayout;
import java.awt.Font;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javalx.data.products.P2;
import prefuse.Constants;
import prefuse.Display;
import prefuse.Visualization;
import prefuse.action.ActionList;
import prefuse.action.RepaintAction;
import prefuse.action.assignment.ColorAction;
import prefuse.action.assignment.FontAction;
import prefuse.action.layout.Layout;
import prefuse.controls.Control;
import prefuse.controls.MouseCoordinatesControl;
import prefuse.controls.NeighborHighlightControl;
import prefuse.controls.WheelZoomControl;
import prefuse.controls.ZoomToFitControl;
import prefuse.data.Node;
import prefuse.data.expression.parser.ExpressionParser;
import prefuse.render.DefaultRendererFactory;
import prefuse.render.RoutedEdgeRenderer;
import prefuse.visual.VisualItem;
import rreil.disassembler.Instruction;
import rreil.lang.RReilAddr;
import ycomb.p9.binary.cfg.controller.BasicBlockHoverControl;
import ycomb.p9.binary.cfg.controller.BasicBlockToolTipControl;
import ycomb.p9.binary.cfg.controller.DraggingControl;
import ycomb.p9.binary.cfg.controller.PanningControl;
import ycomb.p9.binary.cfg.display.renderer.BaseInstructionRenderer;
import ycomb.p9.binary.cfg.display.renderer.BasicBlockRenderer;
import ycomb.p9.binary.cfg.display.renderer.InstructionRenderer;
import ycomb.p9.binary.cfg.graph.BasicBlock;
import ycomb.p9.binary.cfg.graph.Cfg;

public class CfgView extends Display {

  public static final String $Font = "font";
  public static final String $Draw = "draw";
  public static final String $Layout = "layout";
  public static final String $Relayout = "relayout";
  public static final String $Repaint = "repaint";
  public static final String $Graph = "graph";
  public static final String $Nodes = $Graph + ".nodes";
  public static final String $Edges = $Graph + ".edges";
  private final Cfg cfg;
  private final Visualization visualization = m_vis;
  private final BaseInstructionRenderer instructionRenderer;
  private Map<RReilAddr, P2<Node, Integer>> basicBlocks;

  public CfgView(Cfg cfg, BaseInstructionRenderer instructionRenderer) {
    super(new Visualization());
    this.cfg = cfg;
    this.instructionRenderer = instructionRenderer;
    visualization.add($Graph, cfg);
    cfg.getNodeTable().addColumns(MouseCoordinatesControl.mousePositionSchema);
    cfg.getNodeTable().addColumns(InstructionsSearcher.searcherSchema);
    silenceParserMessages();
    initGraphDisplay();
    setBackground(ColorConfig.$Background);
    setForeground(ColorConfig.$Foreground);
  }

  public Cfg getCfg() {
    return cfg;
  }

  public InstructionRenderer getInstructionRenderer() {
    return instructionRenderer;
  }

  public void panToInstruction(RReilAddr address) {
    if (basicBlocks == null) {
      initBasicBlocksMap();
    }
    P2<Node, Integer> location = basicBlocks.get(address);
    if (location == null) {
      return;
    }
    VisualItem visualNode = visualization.getVisualItem($Nodes, location._1());
    BasicBlockRenderer renderer = (BasicBlockRenderer) visualNode.getRenderer();
    renderer.pantToLine(location._2(), visualNode, this, 1000);
    renderer.highlightLine(location._2(), visualNode, this, 3000);
  }

  private void initBasicBlocksMap() {
    basicBlocks = new HashMap<>();
    for (Iterator<?> nodes = cfg.nodes(); nodes.hasNext();) {
      Node node = (Node) nodes.next();
      int index = 0;
      BasicBlock instructions = (BasicBlock) node.get(Cfg.$Block);
      for (Instruction insn : instructions) {
        RReilAddr address = insn.address();
        basicBlocks.put(address, new P2<>(node, index));
        index++;
      }
    }
  }

  private void initGraphDisplay() {
    initNodeAndEdgeRenderers();
    initDrawAction();
    initFontAction();
    initLayoutAction();
    initRelayoutAction();
    initDisplay();
    setLayout(new BorderLayout());
    runInitialActions();
  }

  private void initDisplay() {
    setHighQuality(true);
    addControlListener(new DraggingControl());
    addControlListener(new PanningControl());
    addControlListener(new WheelZoomControl());
    ZoomToFitControl zoomToFitControl = new ZoomToFitControl(Control.MIDDLE_MOUSE_BUTTON);
    zoomToFitControl.setMargin(10);
    addControlListener(zoomToFitControl);
    addControlListener(new NeighborHighlightControl()); // no need to repaint here, the control below will do the repaint
    addControlListener(new MouseCoordinatesControl($Repaint));
    BasicBlockToolTipControl toolTipController = new BasicBlockToolTipControl(cfg);
    addControlListener(toolTipController);
    addControlListener(new BasicBlockHoverControl(toolTipController, Cfg.$Block));
  }

  private void initNodeAndEdgeRenderers() {
    BasicBlockRenderer nodeRenderer
            = new BasicBlockRenderer(instructionRenderer, Cfg.$Block,
                    MouseCoordinatesControl.mousePositionKey, InstructionsSearcher.$SearchResult);
    RoutedEdgeRenderer edgeRenderer = new RoutedEdgeRenderer(Constants.EDGE_TYPE_POLYLINE_CURVE);
    edgeRenderer.setUpdateRoutePointsOnNodeMove(false);
    edgeRenderer.setDefaultLineWidth(1.5);
    edgeRenderer.setArrowHeadSize(10, 13);
    visualization.setRendererFactory(new DefaultRendererFactory(nodeRenderer, edgeRenderer));
  }

  private void initDrawAction() {
    ColorAction edgesDrawArrow = new ColorAction($Edges, VisualItem.FILLCOLOR, ColorConfig.$Edge);
    edgesDrawArrow.add(VisualItem.HIGHLIGHT, ColorConfig.$EdgeHighlight);
    ColorAction edgesDrawLine = new ColorAction($Edges, VisualItem.STROKECOLOR, ColorConfig.$Edge);
    edgesDrawLine.add(VisualItem.HIGHLIGHT, ColorConfig.$EdgeHighlight);
    ActionList draw = new ActionList();
    draw.add(new ColorAction($Nodes, VisualItem.STROKECOLOR, ColorConfig.$NodeBorder));
    draw.add(new ColorAction($Nodes, VisualItem.TEXTCOLOR, ColorConfig.$NodeFg));
    draw.add(new NodesFillColorAction());
    draw.add(edgesDrawArrow);
    draw.add(edgesDrawLine);
    visualization.putAction($Draw, draw);
    ActionList redraw = new ActionList();
    redraw.add(draw);
    redraw.add(new RepaintAction());
    visualization.putAction($Repaint, redraw);
  }

  private void initFontAction() {
    FontAction fontAction = new FontAction();
    fontAction.setDefaultFont(new Font(Font.MONOSPACED, Font.PLAIN, 11));
    visualization.putAction($Font, fontAction);
  }

  private void initLayoutAction() {
    ActionList layout = new ActionList();
    layout.add(getCurrentLayout());
    layout.add(new TranslateAction(40, 40, this));
    layout.add(new RepaintAction());
    visualization.putAction($Layout, layout);
  }

  private void initRelayoutAction() {
    ActionList layout = new ActionList();
    layout.add(getCurrentLayout());
    layout.add(new RepaintAction());
    visualization.putAction($Relayout, layout);
  }

  public void relayout() {
    visualization.run($Relayout);
  }

  private Layout getCurrentLayout () {
    // TODO: see how to get the currently selected layout and how to set it.
    return new CfgLayout($Graph);
  }
  
  private void runInitialActions() {
    visualization.run($Draw);
    visualization.run($Font);
    visualization.run($Layout);
  }

  /**
   * Silence Prefuse parser info messages.
   */
  private static void silenceParserMessages() {
    Logger logger = Logger.getLogger(ExpressionParser.class.getName());
    logger.setLevel(Level.SEVERE);
  }

  public static class NodesFillColorAction extends ColorAction {

    public NodesFillColorAction() {
      super($Nodes, VisualItem.FILLCOLOR);
    }

    @Override
    public int getColor(VisualItem item) {
      if (item.isHover()) {
        return ColorConfig.$FocusBg;
      } else if (item.isHighlighted()) {
        return ColorConfig.$DOIBg;
      } else if (item.isFixed()) {
        return ColorConfig.$SearchBg;
      } else {
        return ColorConfig.$NodeBg;
      }
    }

  }

}
