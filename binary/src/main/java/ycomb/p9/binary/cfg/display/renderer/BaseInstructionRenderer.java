package ycomb.p9.binary.cfg.display.renderer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.font.TextAttribute;
import java.awt.geom.Rectangle2D;
import java.text.AttributedString;
import java.util.ArrayList;
import java.util.List;

import rreil.disassembler.Instruction;
import ycomb.p9.binary.cfg.display.ColorConfig;

/**
 *
 * @author Raphael Dümig
 */
public abstract class BaseInstructionRenderer implements InstructionRenderer {

  protected Font font;
  protected Color textColor;

  protected int addressWidth;
  protected int opcodeWidth;
  protected int mnemonicWidth;
  protected int operandsWidth;

  protected int separatorWidth;
  protected int smallSeparatorWidth;

  protected Dimension textBlockDimension;

  protected boolean opcodeDisabled;
  protected boolean displayNumbersInHex;

  public BaseInstructionRenderer () {
    this(null, null, true);
  }

  public BaseInstructionRenderer (boolean displayNumbersInHex) {
    this(null, null, displayNumbersInHex);
  }

  public BaseInstructionRenderer (Font font, Color textColor, boolean displayNumbersInHex) {
    this.font = font;
    this.textColor = textColor;
    this.opcodeDisabled = false;
    this.displayNumbersInHex = displayNumbersInHex;

    this.separatorWidth = 0;
    this.smallSeparatorWidth = 0;
  }

  public void setColumnWidths (int addressColumnWidth, int opcodeColumnWidth, int mnemonicColumnWidth,
      int operandsColumnWidth) {
    this.addressWidth = addressColumnWidth;
    this.opcodeWidth = opcodeColumnWidth;
    this.mnemonicWidth = mnemonicColumnWidth;
    this.operandsWidth = operandsColumnWidth;
  }

  public void disableOpcode (boolean disable) {
    opcodeDisabled = disable;
  }

  public void setTextColor (Color color) {
    this.textColor = color;
  }

  public void setFont (Font font) {
    this.font = font;
  }

  private Dimension getAddressDimension (Graphics2D g, Instruction insn) {
    return getStringDimension(g, font, getAddress(insn));
  }

  private Dimension getOpcodeDimension (Graphics2D g, Instruction insn) {
    return getStringDimension(g, font, getOpcode(insn));
  }

  private Dimension getMnemonicDimension (Graphics2D g, Instruction insn) {
    return getStringDimension(g, font, getMnemonic(insn));
  }

  private Dimension getOperandsDimension (Graphics2D g, Instruction insn) {
    return getStringDimension(g, font, getOperands(insn, displayNumbersInHex));
  }

  public void calibrateWidths (Graphics2D g, Iterable<Instruction> insns) {
    Dimension addressDimension = new Dimension(0, 0);
    Dimension opcodeDimension = new Dimension(0, 0);
    Dimension mnemonicDimension = new Dimension(0, 0);
    Dimension operandsDimension = new Dimension(0, 0);

    Dimension instAddressDim;
    Dimension instOpcodeDim;
    Dimension instMnemonicDim;
    Dimension instOperandsDim;

    for (Instruction insn : insns) {
      instAddressDim = getAddressDimension(g, insn);
      addressDimension.setSize(
          Math.max(addressDimension.getWidth(), instAddressDim.getWidth()),
          addressDimension.getHeight() + instAddressDim.getHeight()
          );

      if (!opcodeDisabled) {
        instOpcodeDim = getOpcodeDimension(g, insn);
        opcodeDimension.setSize(
            Math.max(opcodeDimension.getWidth(), instOpcodeDim.getWidth()),
            opcodeDimension.getHeight() + instOpcodeDim.getHeight()
            );
      }

      instMnemonicDim = getMnemonicDimension(g, insn);
      mnemonicDimension.setSize(
          Math.max(mnemonicDimension.getWidth(), instMnemonicDim.getWidth()),
          mnemonicDimension.getHeight() + instMnemonicDim.getHeight()
          );

      instOperandsDim = getOperandsDimension(g, insn);
      operandsDimension.setSize(
          Math.max(operandsDimension.getWidth(), instOperandsDim.getWidth()),
          operandsDimension.getHeight() + instOperandsDim.getHeight()
          );
    }

    addressWidth = (int) Math.ceil(addressDimension.getWidth());
    opcodeWidth = (int) Math.ceil(opcodeDimension.getWidth());
    mnemonicWidth = (int) Math.ceil(mnemonicDimension.getWidth());
    operandsWidth = (int) Math.ceil(operandsDimension.getWidth());

    smallSeparatorWidth = (int) Math.ceil(font.getMaxCharBounds(g.getFontRenderContext()).getWidth());
    separatorWidth = 2 * smallSeparatorWidth;

    // calculate the height of the textblock by determining the maximum height of the columns
    int maxHeight = (int) Math.ceil(Math.max(
        addressDimension.getHeight(),
        Math.max(opcodeDimension.getHeight(),
            Math.max(mnemonicDimension.getHeight(), operandsDimension.getHeight()))
        ));

    textBlockDimension = new Dimension(getInstructionWidth(), maxHeight);
  }

  public int getInstructionWidth () {
    return addressWidth + separatorWidth +
      (opcodeDisabled ? 0 : opcodeWidth + separatorWidth) +
      mnemonicWidth + smallSeparatorWidth +
      operandsWidth;
  }

  public int getAddressSeparatorPosition () {
    return addressWidth + separatorWidth / 2;
  }

  public int getOpcodeSeparatorPosition () {
    return (opcodeDisabled ? 0 : addressWidth + separatorWidth + opcodeWidth + separatorWidth / 2);
  }

  public boolean opcodeIsDisabled () {
    return opcodeDisabled;
  }

  public void renderInstruction (Graphics2D g, Instruction insn, String searchQuery, boolean selectedHitInLine) {
    String addressString = getAddress(insn).trim();
    String opcodeString = getOpcode(insn).trim();
    String mnemonicString = getMnemonic(insn).trim();
    String operandsString = getOperands(insn, displayNumbersInHex).trim();

    int addressOffset = 0;
    int opcodeOffset = addressOffset + addressWidth + separatorWidth;
    int mnemonicOffset = opcodeOffset + (opcodeDisabled ? 0 : opcodeWidth + separatorWidth);
    int operandsOffset = mnemonicOffset + mnemonicWidth + smallSeparatorWidth;


    if (!addressString.isEmpty()) {
      // size of the unformatted string including whitespace-separator
      AttributedString styledElement = new AttributedString(addressString);
      applyFontAttributes(styledElement);
      styledElement.addAttribute(TextAttribute.FOREGROUND, ColorConfig.$Address);
      if (searchQuery != null)
        highlightSearchResults(addressString, styledElement, searchQuery, selectedHitInLine);
      g.drawString(styledElement.getIterator(), addressOffset, 0);
    }

    if (!opcodeDisabled && !opcodeString.isEmpty()) {
      AttributedString styledElement = new AttributedString(opcodeString);
      applyFontAttributes(styledElement);
      styledElement.addAttribute(TextAttribute.FOREGROUND, ColorConfig.$Opcode);
      styledElement.addAttribute(TextAttribute.POSTURE, TextAttribute.POSTURE_OBLIQUE);
      if (searchQuery != null)
        highlightSearchResults(opcodeString, styledElement, searchQuery, selectedHitInLine);
      g.drawString(styledElement.getIterator(), opcodeOffset, 0);
    }

    if (!mnemonicString.isEmpty()) {
      AttributedString styledElement = new AttributedString(mnemonicString);
      applyFontAttributes(styledElement);
      styledElement.addAttribute(TextAttribute.FOREGROUND, ColorConfig.$Mnemonic);
      if (searchQuery != null)
        highlightSearchResults(mnemonicString, styledElement, searchQuery, selectedHitInLine);
      g.drawString(styledElement.getIterator(), mnemonicOffset, 0);
    }

    if (!operandsString.isEmpty()) {
      AttributedString styledElement = new AttributedString(operandsString);
      applyFontAttributes(styledElement);
      styleOperands(styledElement, operandsString, 0, font);
      if (searchQuery != null)
        highlightSearchResults(operandsString, styledElement, searchQuery, selectedHitInLine);
      g.drawString(styledElement.getIterator(), operandsOffset, 0);
    }
  }

  private void applyFontAttributes (AttributedString text) {
    text.addAttribute(TextAttribute.FAMILY, font.getFamily());
    text.addAttribute(TextAttribute.SIZE, font.getSize());
    text.addAttribute(TextAttribute.FOREGROUND, textColor);
  }

  private static List<TextRange> getOccurrences (String text, String search) {
    List<TextRange> hits = new ArrayList<>();
    int length = search.length();
    int index = 0;
    while (index >= 0) {
      index = text.indexOf(search, index);
      if (index < 0) {
        break;
      }
      hits.add(new TextRange(index, index + length));
      index++;
    }
    return hits;
  }

  private static void highlightSearchResults (String text, AttributedString styledText, String searchQuery,
      boolean selectedHitInLine) {
    if (selectedHitInLine) {
      styledText.addAttribute(TextAttribute.BACKGROUND, ColorConfig.$TextHighlightBackground.brighter().brighter());
    }
    List<TextRange> hits = getOccurrences(text, searchQuery);
    if (!hits.isEmpty()) {
      for (TextRange range : hits) {
        styledText.addAttribute(TextAttribute.BACKGROUND, ColorConfig.$TextHighlightBackground, range.begin, range.end);
      }
    }
  }

  private static Dimension getStringDimension (Graphics2D g, Font font, String text) {
    Rectangle2D bbox = font.getStringBounds(text.trim(), g.getFontRenderContext());
    Dimension stringDimension = new Dimension();
    stringDimension.setSize(bbox.getWidth(), bbox.getHeight());
    return stringDimension;
  }

}
