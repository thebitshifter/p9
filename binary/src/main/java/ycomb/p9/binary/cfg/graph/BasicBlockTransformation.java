package ycomb.p9.binary.cfg.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import prefuse.data.Node;

/**
 * Compact a CFG such that all straight line instructions without branches
 * form a basic block.
 *
 * @author Bogdan Mihaila
 */
public class BasicBlockTransformation {
  private final Set<Node> blockLeaders = new HashSet<Node>();
  private final Map<Node, Node> blocks = new HashMap<Node, Node>();
  private Cfg newCfg;

  public static Cfg run (Cfg cfg) {
    BasicBlockTransformation transformer = new BasicBlockTransformation();
    return transformer.apply(cfg);
  }

  @SuppressWarnings("unchecked")
  private Cfg apply (Cfg cfg) {
    newCfg = new Cfg(cfg.getAnalysis(), cfg.getCallString());
    Iterator<Node> nodes = cfg.nodes();
    while (nodes.hasNext()) {
      Node n = nodes.next();
      int inDegree = n.getInDegree();
      if (inDegree == 0 || inDegree > 1)
        addAsLeader(n);
      int outDegree = n.getOutDegree();
      if (outDegree > 1) {
        Iterator<Node> succs = n.outNeighbors();
        while (succs.hasNext()) {
          Node succ = succs.next();
          addAsLeader(succ);
        }
      }
    }
    for (Node leader : blockLeaders) {
      Node other = blocks.get(leader);
      inlineBlock(leader, leader, asBasicBlock(other));
    }
    return newCfg;
  }

  @SuppressWarnings("unchecked")
  private void inlineBlock (Node leader, Node currentNode, BasicBlock block) {
    BasicBlock other = asBasicBlock(currentNode);
    assert other != null;
    assert block != other;
    block.addAll(other);
    Iterator<Node> succs = currentNode.outNeighbors();
    while (succs.hasNext()) {
      Node succ = succs.next();
      if (isLeader(succ))
        newCfg.addEdge(blocks.get(leader), blocks.get(succ));
      else
        inlineBlock(leader, succ, block);
    }
  }

  private static BasicBlock asBasicBlock (Node node) {
    return (BasicBlock) node.get(Cfg.$Block);
  }

  private void addAsLeader (Node node) {
    if (blockLeaders.contains(node))
      return;
    blockLeaders.add(node);
    Node other = newCfg.addNode();
    blocks.put(node, other);
    other.set(Cfg.$Block, new BasicBlock());
  }

  private boolean isLeader (Node node) {
    return blockLeaders.contains(node);
  }
}
