package ycomb.p9.binary.cfg.controller;

import java.awt.event.ActionEvent;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JPopupMenu;

import org.openide.util.ImageUtilities;

import prefuse.visual.NodeItem;
import rreil.disassembler.Instruction;
import rreil.lang.RReilAddr;
import rreil.lang.Rhs.Rvar;
import rreil.lang.lowlevel.LowLevelRReil;
import ycomb.p9.binary.cfg.InstructionsHelper;
import ycomb.p9.binary.cfg.controller.events.CfgViewEventBus;
import ycomb.p9.binary.cfg.controller.events.CfgViewEventBus.CfgFocusAddressEvent;
import ycomb.p9.binary.cfg.controller.events.CfgViewEventBus.CfgFocusAddressInNativeCfgEvent;
import ycomb.p9.binary.cfg.controller.events.CfgViewEventBus.CfgFocusAddressInRReilCfgEvent;
import ycomb.p9.binary.cfg.display.CfgComponent;
import ycomb.p9.binary.cfg.graph.Cfg;
import bindead.analyses.fixpoint.ProgramCtx;

public class RReilContextMenu extends BasicBlockContextMenu {
  private static final ImageIcon showInOtherIcon = ImageUtilities.loadImageIcon(iconPath, false);

  public RReilContextMenu (Cfg cfg, String functionName, String blockSchemaID) {
    super(cfg, functionName, blockSchemaID);
  }

  @Override protected JPopupMenu handleCfgNode (NodeItem item) {
    Instruction selectedInstruction = getSelectedInstruction(item);
    RReilAddr selectedAddress = selectedInstruction.address();
    JPopupMenu menu = new JPopupMenu();
    Set<Rvar> variables = InstructionsHelper.getOccurringVariables(((LowLevelRReil) selectedInstruction).toRReil());
    addShowDomainAction(selectedAddress, variables, menu);
    addCopyToClipboardActions(item, selectedInstruction, menu);
    if (haveNativeCfg())
      addShowInOtherAction(selectedAddress.withOffset(0), menu);
    addFollowCallsReturnsActions(selectedAddress, menu);
    return menu;
  }

  private boolean haveNativeCfg() {
    return analysis.getAnalysis().getBinaryCode() != null;
  }

  private void addShowInOtherAction (final RReilAddr address, JPopupMenu menu) {
    Action action = new AbstractAction() {
      @Override public void actionPerformed (ActionEvent e) {
        CfgFocusAddressEvent event = new CfgFocusAddressInNativeCfgEvent(analysis, new ProgramCtx(callString, address));
        CfgViewEventBus.getInstance().publish(event);
      }
    };
    String architecture = analysis.getAnalysis().getBinaryCode().getBinary().getArchitectureName();
    action.putValue(Action.SMALL_ICON, showInOtherIcon);
    action.putValue(Action.NAME, Bundle.CTL_ShowInCfgAction(architecture));
    action.putValue(Action.SHORT_DESCRIPTION, Bundle.HINT_ShowInCfgAction(architecture));
    menu.add(action);
  }

  private void addFollowCallsReturnsActions (RReilAddr selectedAddress, JPopupMenu menu) {
    if (calls.containsRReilKey(selectedAddress)) {
      ProgramCtx target = calls.getRReil(selectedAddress);
      Action action = buildFollowCallsReturnsAction(target);
      CfgComponent.setCfgOpenActionDescription(action, "RREIL", "called ");
      menu.add(action);
    }
    if (returns.containsRReilKey(selectedAddress)) {
      ProgramCtx target = returns.getRReil(selectedAddress);
      Action action = buildFollowCallsReturnsAction(target);
      CfgComponent.setCfgOpenActionDescription(action, "RREIL", "returned to ");
      menu.add(action);
    }
  }

  private Action buildFollowCallsReturnsAction (final ProgramCtx target) {
    Action action = new AbstractAction() {
      @Override public void actionPerformed (ActionEvent e) {
        CfgFocusAddressEvent event = new CfgFocusAddressInRReilCfgEvent(analysis, target);
        CfgViewEventBus.getInstance().publish(event);
      }
    };
    return action;
  }

}
