package ycomb.p9.binary.cfg.controller;

import java.awt.event.ActionEvent;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JPopupMenu;

import org.openide.util.ImageUtilities;

import prefuse.visual.NodeItem;
import rreil.disassembler.Instruction;
import rreil.lang.RReil;
import rreil.lang.RReilAddr;
import rreil.lang.Rhs.Rvar;
import ycomb.p9.binary.cfg.InstructionsHelper;
import ycomb.p9.binary.cfg.controller.events.CfgViewEventBus;
import ycomb.p9.binary.cfg.controller.events.CfgViewEventBus.CfgFocusAddressEvent;
import ycomb.p9.binary.cfg.controller.events.CfgViewEventBus.CfgFocusAddressInNativeCfgEvent;
import ycomb.p9.binary.cfg.controller.events.CfgViewEventBus.CfgFocusAddressInRReilCfgEvent;
import ycomb.p9.binary.cfg.display.CfgComponent;
import ycomb.p9.binary.cfg.graph.Cfg;
import bindead.analyses.BinaryCodeCache;
import bindead.analyses.RReilCodeCache;
import bindead.analyses.fixpoint.ProgramCtx;
import bindead.analyses.fixpoint.TransitionSystem;
import bindead.analyses.fixpoint.TransitionSystem.ProceduralTransitions;
import bindead.domainnetwork.interfaces.ProgramPoint;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

public class NativeContextMenu extends BasicBlockContextMenu {
  private static final ImageIcon showInOtherIcon = ImageUtilities.loadImageIcon(iconPath, false);
  private Multimap<RReilAddr, RReil> instructionsMapping;

  public NativeContextMenu (Cfg cfg, String functionName, String blockSchemaID) {
    super(cfg, functionName, blockSchemaID);
    buildRReilInstructionsMapping();
  }

  private void buildRReilInstructionsMapping () {
    instructionsMapping = HashMultimap.create();
    TransitionSystem transitionSystem = analysis.getAnalysis().getTransitionSystem();
    RReilCodeCache rreilcode = analysis.getAnalysis().getRReilCode();
    ProceduralTransitions procedure = transitionSystem.getTransitionsFor(callString);
    for (Entry<ProgramPoint, ProgramPoint> transition : procedure.getLocalTransitions().entries()) {
      RReil insn = rreilcode.getInstruction(transition.getKey().getAddress());
      RReilAddr nativeAddress = insn.getRReilAddress().withOffset(0);
      instructionsMapping.put(nativeAddress, insn);
    }
    // workaround to also insert call instructions to the map as they are not present in the local transitions
    Multimap<ProgramPoint, ProgramCtx> calls = procedure.getCalls();
    if (!calls.isEmpty()) {
      for (ProgramPoint callSite : calls.keys()) {
        RReil call = rreilcode.getInstruction(callSite.getAddress());
        RReilAddr nativeAddress = call.getRReilAddress().withOffset(0);
        instructionsMapping.put(nativeAddress, call);
      }
    }
  }

  @Override protected JPopupMenu handleCfgNode (NodeItem item) {
    Instruction selectedInstruction = getSelectedInstruction(item);
    RReilAddr selectedAddress = selectedInstruction.address();
    JPopupMenu menu = new JPopupMenu();
    Set<Rvar> variables = InstructionsHelper.getOccurringVariables(selectedInstruction, analysis.getAnalysis().getPlatform());
    addShowDomainAction(selectedAddress, variables, menu);
    addCopyToClipboardActions(item, selectedInstruction, menu);
    addShowInOtherAction(selectedAddress, menu);
    addFollowCallsAndReturnsActions(selectedAddress, menu);
    return menu;
  }

  private void addShowInOtherAction (final RReilAddr address, JPopupMenu menu) {
    Action action = new AbstractAction() {
      @Override public void actionPerformed (ActionEvent e) {
        CfgFocusAddressEvent event = new CfgFocusAddressInRReilCfgEvent(analysis, new ProgramCtx(callString, address));
        CfgViewEventBus.getInstance().publish(event);
      }
    };
    String cfgType = "RREIL";
    action.putValue(Action.SMALL_ICON, showInOtherIcon);
    action.putValue(Action.NAME, Bundle.CTL_ShowInCfgAction(cfgType));
    action.putValue(Action.SHORT_DESCRIPTION, Bundle.HINT_ShowInCfgAction(cfgType));
    menu.add(action);
  }

  private void addFollowCallsAndReturnsActions (final RReilAddr address, JPopupMenu menu) {
    if (calls.containsNativeKey(address)) {
      ProgramCtx target = calls.getNative(address);
      Action action = buildFollowCallsReturnsAction(target);
      BinaryCodeCache binaryCodeView = analysis.getAnalysis().getBinaryCode();
      String architecture = binaryCodeView != null ? binaryCodeView.getBinary().getArchitectureName() : "RREIL";
      CfgComponent.setCfgOpenActionDescription(action, architecture, "called ");
      menu.add(action);
    }
    if (returns.containsNativeKey(address)) {
      ProgramCtx target = returns.getNative(address);
      Action action = buildFollowCallsReturnsAction(target);
      BinaryCodeCache binaryCodeView = analysis.getAnalysis().getBinaryCode();
      String architecture = binaryCodeView != null ? binaryCodeView.getBinary().getArchitectureName() : "RREIL";
      CfgComponent.setCfgOpenActionDescription(action, architecture, "returned to ");
      menu.add(action);
    }
  }

  private Action buildFollowCallsReturnsAction (final ProgramCtx target) {
    Action action = new AbstractAction() {
      @Override public void actionPerformed (ActionEvent e) {
        CfgFocusAddressEvent event = new CfgFocusAddressInNativeCfgEvent(analysis, target);
        CfgViewEventBus.getInstance().publish(event);
      }
    };
    return action;
  }

}
