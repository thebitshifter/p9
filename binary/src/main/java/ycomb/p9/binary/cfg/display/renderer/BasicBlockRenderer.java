package ycomb.p9.binary.cfg.display.renderer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.util.HashMap;
import java.util.Map;

import prefuse.Display;
import prefuse.render.AbstractShapeRenderer;
import prefuse.util.ColorLib;
import prefuse.util.FontLib;
import prefuse.util.GraphicsLib;
import prefuse.visual.VisualItem;
import rreil.disassembler.Instruction;
import ycomb.p9.binary.cfg.display.ColorConfig;
import ycomb.p9.binary.cfg.display.InstructionsSearcher.CfgBlockSearchHits;
import ycomb.p9.binary.cfg.display.Overview;
import ycomb.p9.binary.cfg.display.RunAfter;
import ycomb.p9.binary.cfg.graph.BasicBlock;
import bindead.debug.StringHelpers;

/**
 * A renderer for instruction basic blocks.
 */
public class BasicBlockRenderer extends AbstractShapeRenderer {
  private final String blockSchemaID;
  private final String mousePositionSchemaID;
  private final String searchHitsSchemaID;
  private BasicBlock block;
  private float borderWidth = 3;
  private int horizontalInsets;
  private int verticalInsets;
  private double arcWidth;
  private double arcHeight;
  private FontMetrics fontMetrics;
  private Font font;
  private final RoundRectangle2D node = new RoundRectangle2D.Double();
  private final RoundRectangle2D dropShadow = new RoundRectangle2D.Double();
  private final RoundRectangle2D highlight = new RoundRectangle2D.Double();
  private final Rectangle2D shape = new Rectangle2D.Double();
  private final Dimension boundingBox = new Dimension();
  private final BasicStroke borderStroke = new BasicStroke(borderWidth);
  private final BasicStroke addressSeparatorStroke = new BasicStroke(1);
  private final BasicStroke nativeInsnSeparatorStroke = new BasicStroke(1, BasicStroke.CAP_BUTT,
    BasicStroke.JOIN_BEVEL, 0, new float[] {5}, 0);
  private final BaseInstructionRenderer renderer;
  private final Map<VisualItem, HighlightLineAnimation> highlighters = new HashMap<VisualItem, HighlightLineAnimation>();
  private int addressColumnSize;
  private int opcodeColumnSize;
  private int mnemonicColumnSize;
  private boolean printNumbersInHex;
  private VisualItem currentItem;

  public BasicBlockRenderer (BaseInstructionRenderer renderer, String blockSchemaID, String mousePositionSchemaID,
      String searchHitsSchemaID) {
    this.renderer = renderer;
    this.blockSchemaID = blockSchemaID;
    this.mousePositionSchemaID = mousePositionSchemaID;
    this.searchHitsSchemaID = searchHitsSchemaID;
    setRoundedCorners(10);
    setInsets(10, 5);
    setPrintNumbersInHex(true);
  }

  public void setRoundedCorners (int radius) {
    setRoundedCorners(radius, radius);
  }

  public void setRoundedCorners (int arcWidth, int arcHeight) {
    this.arcWidth = arcWidth;
    this.arcHeight = arcHeight;
  }

  public void setBorderWidth (float borderWidth) {
    this.borderWidth = borderWidth;
  }

  public float getBorderWidth () {
    return borderWidth;
  }

  public void setInsets (int horizontal, int vertical) {
    horizontalInsets = horizontal;
    verticalInsets = vertical;
  }

  public int getHorizontalInset () {
    return horizontalInsets;
  }

  public int getVerticalInset () {
    return verticalInsets;
  }

  public boolean isPrintNumbersInHex () {
    return printNumbersInHex;
  }

  public void setPrintNumbersInHex (boolean printNumbersInHex) {
    this.printNumbersInHex = printNumbersInHex;
  }

  @Override public void render (Graphics2D g, VisualItem item) {
    double zoomFactor = item.getVisualization().getDisplay(0).getScale();
    if (zoomFactor < 0.20 || item.isInGroup(Overview.overviewGroup))
      renderOverviewQuality(g, item);
    else
      renderNormalQuality(g, item);
  }

  private void renderOverviewQuality (Graphics2D g, VisualItem item) {
    Shape shape = getShape(item);
    if (shape == null)
      return;
    drawBackground(g);
    drawBorder(g);
  }

  private void renderNormalQuality (Graphics2D g, VisualItem item) {
    Shape shape = getShape(item);
    if (shape == null)
      return;
    
    renderer.calibrateWidths(g, block);
    updateBlockDimensions();
    
    drawShadow(g);
    drawBackground(g);
    
    // draw a vertical separator after the addresses
    drawColumnSeparator(g, renderer.getAddressSeparatorPosition());
    
    // draw a vertical separator after the opcode if the opcode is visible
    if ( !renderer.opcodeIsDisabled() )
      drawColumnSeparator(g, renderer.getOpcodeSeparatorPosition());
    
    drawText(g);
    drawBorder(g);
  }

  @Override protected Shape getRawShape (VisualItem item) {
    installItemInRenderer(item);
    return shape;
  }

  private void installItemInRenderer (VisualItem item) {
    currentItem = item;
    setBlock();
    setFont();
    
    renderer.calibrateWidths(DEFAULT_GRAPHICS, block);
    updateBlockDimensions();
  }

  private int getNumberOfLines () {
    return block.size();
  }

  private int getLineHeight () {
    return fontMetrics.getHeight();
  }

  /**
   * Recalculate the geometry of the block, especially height and width.
   * 
   * BaseInstructionRenderer.calibrateWidths() has to be called before
   * this method in order to get correct results.
   */
  private void updateBlockDimensions () {
    double size = currentItem.getSize();
    
    double x = currentItem.getX();
    double y = currentItem.getY();
    // safety checks
    if (Double.isNaN(x) || Double.isInfinite(x))
      x = 0;
    if (Double.isNaN(y) || Double.isInfinite(y))
      y = 0;
    
    int insnsWidth  = renderer.getInstructionWidth();
    int insnsHeight = getNumberOfLines() * getLineHeight();
    int width =  insnsWidth  + (int) Math.ceil(size * 2 * horizontalInsets);
    int height = insnsHeight + (int) Math.ceil(size * 2 * verticalInsets);
    
    int dx = 4;
    int dy = 4;
    
    node.setRoundRect(x, y, width, height, size * arcWidth, size * arcHeight);
    dropShadow.setRoundRect(x + dx, y + dy, width + dx, height + dy, size * arcWidth, size * arcHeight);
    
    // NOTE: this is a hack to extend the size of the node to also contain the drop shadow. needed for the repaints
    double diffBoundsToContent = 8;
    shape.setRect(x, y, width + diffBoundsToContent, height + diffBoundsToContent);
    
    boundingBox.setSize(insnsWidth, insnsHeight);
  }
  
  private void setBlock () {
    if (currentItem.canGet(blockSchemaID, BasicBlock.class))
      block = (BasicBlock) currentItem.get(blockSchemaID);
  }

  private void setFont () {
    double size = currentItem.getSize();
    font = currentItem.getFont();
    // scale the font as needed
    if (size != 1)
      font = FontLib.getFont(font.getName(), font.getStyle(), size * font.getSize());
    fontMetrics = DEFAULT_GRAPHICS.getFontMetrics(font);
    renderer.setFont(font);
  }

  private void drawColumnSeparator (Graphics2D g, int separatorPos) {
    double size = currentItem.getSize();
    double x = node.getMinX() + horizontalInsets + separatorPos;
    double y = node.getMinY() + size * verticalInsets;
    g.setPaint(ColorConfig.$ColumnSeparator);
    Stroke oldStroke = g.getStroke();
    g.setStroke(addressSeparatorStroke);
    g.drawLine((int) x, (int) y, (int) x, (int) y + boundingBox.height);
    g.setStroke(oldStroke);
  }

  private CfgBlockSearchHits getSearchHits () {
    if (currentItem.canGet(searchHitsSchemaID, CfgBlockSearchHits.class))
      return (CfgBlockSearchHits) currentItem.get(searchHitsSchemaID);
    else
      return null;
  }

  /**
   * Returns the content of the item as a formatted string.
   */
  public String asString (VisualItem item) {
    installItemInRenderer(item); // initializes all the properties in this renderer
    StringBuilder builder = new StringBuilder();
    for (Instruction insn : block) {
      builder.append(renderer.getText(insn, addressColumnSize, opcodeColumnSize, mnemonicColumnSize, printNumbersInHex));
      builder.append("\n");
    }
    builder.setLength(builder.length() - 1);
    return builder.toString();
  }

  /**
   * Returns the content of the specified instruction in the item as a formatted string.
   */
  public String asString (VisualItem item, Instruction insn) {
    installItemInRenderer(item); // initializes all the properties in this renderer
    return renderer.getText(insn, addressColumnSize, opcodeColumnSize, mnemonicColumnSize, printNumbersInHex);
  }

  /**
   * Returns the content of the specified line in the item as a formatted string.
   */
  public String asString (VisualItem item, int line) {
    if (line > block.size())
      return "";
    installItemInRenderer(item); // initializes all the properties in this renderer
    Instruction insn = block.get(line);
    return renderer.getText(insn, addressColumnSize, opcodeColumnSize, mnemonicColumnSize, printNumbersInHex);
  }

  /**
   * Pans the display animated to center on the line number in the item.
   */
  public void pantToLine (int lineNumber, VisualItem item, Display display, int animationDuration) {
    installItemInRenderer(item);
    if (lineNumber < 0 || lineNumber > getNumberOfLines())
      return;
    double size = item.getSize();
    Rectangle2D bounds = item.getBounds();
    double x = bounds.getX() + bounds.getWidth() / 2;
    double y = bounds.getY() + size * verticalInsets + getLineHeight() * lineNumber;
    display.animatePanToAbs(new Point2D.Double(x, y), animationDuration);
  }

  /**
   * Highlights a line in the item for a certain time and then removes the highlight.
   */
  public void highlightLine (int lineNumber, VisualItem item, Display display, int animationDuration) {
    installItemInRenderer(item);
    HighlightLineAnimation animation = new HighlightLineAnimation(lineNumber, item, display, animationDuration);
    animation.start();
  }

  /**
   * Return the line in the item that is hovered or clicked with the mouse.
   */
  public int getSelectedLine (VisualItem item) {
    installItemInRenderer(item);
    return getSelectedLine();
  }

  private int getSelectedLine () {
    if (!currentItem.canGet(mousePositionSchemaID, Point2D.class))
      return -1;
    Point2D position = (Point2D) currentItem.get(mousePositionSchemaID);
    if (position == null || position.getX() < 0 && position.getY() < 0)
      return -1;
    
    double size = currentItem.getSize();
    double positionInText = position.getY() - size * verticalInsets;
    return (int) Math.floor(positionInText / getLineHeight());
  }

  private int getHighlightedLine () {
    HighlightLineAnimation highlighter = highlighters.get(currentItem);
    if (highlighter != null)
      return highlighter.getHighlightedLine();
    return -1;
  }

  private void drawRectangleHighlight (Graphics2D g, double x, double y, Color highlightColor) {
    updateBlockDimensions();
    int stringHeight = getLineHeight();
    g.setPaint(highlightColor);
    int arcWidth = 10;
    int arcHeight = 10;
    highlight.setRoundRect(x, y - stringHeight + 3, boundingBox.width, stringHeight, arcWidth, arcHeight);
    g.fill(highlight);
    g.setPaint(highlightColor.darker().darker());
    g.draw(highlight);
  }

  private void drawInsnGroupSeparator (Graphics2D g, double x, double y) {
    g.setPaint(ColorConfig.$NativeInsnSeparator);
    Stroke oldStroke = g.getStroke();
    g.setStroke(nativeInsnSeparatorStroke);
    g.drawLine((int) x, (int) y, (int) x + boundingBox.width, (int) y);
    g.setStroke(oldStroke);
  }

  private void drawShadow (Graphics2D g) {
    int type = getRenderType(currentItem);
    if (type == RENDER_TYPE_FILL || type == RENDER_TYPE_DRAW_AND_FILL) {
      g.setPaint(ColorConfig.$NodeShadow);
      g.fill(dropShadow);
    }
  }

  /**
   * Uses {@code item.getFillColor()} to retrieve the color for the background.
   */
  private void drawBackground (Graphics2D g) {
    int type = getRenderType(currentItem);
    if (type == RENDER_TYPE_FILL || type == RENDER_TYPE_DRAW_AND_FILL) {
      GraphicsLib.paint(g, currentItem, node, getStroke(currentItem), RENDER_TYPE_FILL);
    }
  }

  /**
   * Uses {@code item.getStrokeColor()} to retrieve the color for the border.
   */
  private void drawBorder (Graphics2D g) {
    int type = getRenderType(currentItem);
    if (type == RENDER_TYPE_DRAW || type == RENDER_TYPE_DRAW_AND_FILL)
      GraphicsLib.paint(g, currentItem, node, borderStroke, RENDER_TYPE_DRAW);
  }

  /**
   * Uses {@code item.getTextColor()} to retrieve the color for the text.
   */
  private void drawText (Graphics2D g) {
    int textColor = currentItem.getTextColor();
    if (ColorLib.alpha(textColor) > 0) {
      renderer.setTextColor(ColorLib.getColor(textColor));
        
      double size = currentItem.getSize();
      double x = node.getMinX() + size * horizontalInsets;
      double y = node.getMinY() + size * verticalInsets + fontMetrics.getAscent();

      int lineNumber = 0;
      int selectedLineNumber = getSelectedLine();
      int highlightedLineNumber = getHighlightedLine();
      CfgBlockSearchHits searchHits = getSearchHits();
      int lineHeight = getLineHeight();
      for (Instruction insn : block) {
        if (lineNumber != 0 && renderer.startsBlock(insn))
          drawInsnGroupSeparator(g, x, y - lineHeight + size * 3);
        if (lineNumber == highlightedLineNumber)
          drawRectangleHighlight(g, x, y, ColorConfig.$RectangleHighlight);
        if (lineNumber == selectedLineNumber)
          drawRectangleHighlight(g, x, y, ColorConfig.$MouseHoverHighlight);
        
        String searchQuery = null;
        boolean selectedHitInLine = false;
        if(searchHits != null) {
          searchQuery = searchHits.searchQuery;
          selectedHitInLine = (lineNumber == searchHits.lineIndexOfSelectedHit);
        }
        g.translate(x, y);
        renderer.renderInstruction(g, insn, searchQuery, selectedHitInLine);
        g.translate(-x, -y);
        
        y = y + lineHeight;
        lineNumber++;
      }
    }
  }

  private String getInstructionText (Instruction insn) {
    return renderer.getText(insn, addressColumnSize, opcodeColumnSize, mnemonicColumnSize, printNumbersInHex);
  }


  private class HighlightLineAnimation {
    private final int lineNumber;
    private final VisualItem item;
    private final Display display;
    private final int animationDuration;

    public HighlightLineAnimation (int lineNumber, VisualItem item, Display display, int animationDuration) {
      this.lineNumber = lineNumber;
      this.item = item;
      this.display = display;
      this.animationDuration = animationDuration;
    }

    public void start () {
      addHighlight();
      RunAfter.delay(animationDuration, new Runnable() {
        @Override public void run () {
          removeHighlight();
        }
      });
    }

    public int getHighlightedLine () {
      return lineNumber;
    }

    private void addHighlight () {
      highlighters.put(item, this);
      display.getVisualization().damageReport(item, item.getBounds());
      display.repaint();
    }

    private void removeHighlight () {
      if (highlighters.get(item) == this) {
        highlighters.remove(item);
        display.getVisualization().damageReport(item, item.getBounds());
        display.repaint();
      }
    }

  }

}
