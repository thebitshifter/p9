package ycomb.p9.binary.cfg.graph;

import javalx.data.Option;
import prefuse.data.Graph;
import prefuse.data.Schema;
import rreil.lang.RReilAddr;
import bindead.analyses.BinaryCodeCache;
import bindead.analyses.fixpoint.CallString;
import bindead.analyses.fixpoint.CallString.Transition;
import binparse.Binary;
import binparse.Symbol;

/**
 * A control flow graph.
 *
 * @author Bogdan Mihaila
 */
public class Cfg extends Graph {
  public static final String $Block = "block";
  public static final Schema $DataSchema = new Schema();
  private final AnalysisResult analysis;
  private final CallString callString;

  static {
    $DataSchema.addColumn($Block, BasicBlock.class);
  }

  public Cfg (AnalysisResult analysis, CallString callString) {
    super(true);
    this.analysis = analysis;
    this.callString = callString;
    getNodeTable().addColumns($DataSchema);
  }

  public static String getName (AnalysisResult analysis, CallString callString) {
    if (callString.isRoot())
      return "[]";
    Transition transition = callString.peek();
    RReilAddr callToCfg = transition.getTarget(); // a procedure is identified by the call entry that it has, thus target here
    BinaryCodeCache binaryCodeView = analysis.getAnalysis().getBinaryCode();
    if (binaryCodeView == null)
      return callToCfg.toString();
    Binary binary = binaryCodeView.getBinary();
    Option<Symbol> symbol = binary.getSymbol(callToCfg.base());
    if (symbol.isSome())
      return symbol.get().toString();
    return callToCfg.toString();
  }

  public String getName () {
    return getName(analysis, callString);
  }

  public AnalysisResult getAnalysis () {
    return analysis;
  }

  public CallString getCallString () {
    return callString;
  }

}
