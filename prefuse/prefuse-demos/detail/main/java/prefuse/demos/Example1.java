package prefuse.demos;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import prefuse.Constants;

import prefuse.Display;
import prefuse.Visualization;
import prefuse.action.ActionList;
import prefuse.action.RepaintAction;
import prefuse.action.assignment.ColorAction;
import prefuse.action.layout.RandomLayout;
import prefuse.controls.DragControl;
import prefuse.controls.NeighborHighlightControl;
import prefuse.controls.PanControl;
import prefuse.controls.ZoomControl;
import prefuse.data.Graph;
import prefuse.data.io.DataIOException;
import prefuse.data.io.GraphMLWriter;
import prefuse.data.io.GraphWriter;
import prefuse.render.DefaultRendererFactory;
import prefuse.render.LabelRenderer;
import prefuse.render.ShapeRenderer;
import prefuse.util.ColorLib;
import prefuse.visual.VisualItem;

public class Example1 {
  public static void main (String[] argv) {

    // -- 1. load the data ------------------------------------------------

    // Here we are manually creating the data structures.  100 nodes are
    // added to the Graph structure.  100 edges are made randomly
    // between the nodes.

    Graph graph = new Graph();

    int numNodes = 10000;

    for (int i = 0; i < numNodes; i++) {
      graph.addNode();
    }

    Random rand = new Random();

    for (int i = 0; i < numNodes; i++) {
      int first = rand.nextInt(numNodes);
      int second = rand.nextInt(numNodes);
      graph.addEdge(first, second);
    }

    // -- 2. the visualization --------------------------------------------
    // We must first creat the Visualization object.
    Visualization vis = new Visualization();

    // Now we add our previously created Graph object to the visualization.
    // The graph gets a textual label so that we can refer to it later on.
    vis.add("graph", graph);

    GraphMLWriter writer = new GraphMLWriter();
    try {
      writer.writeGraph(graph, "/home/mb0/t.graphml");
    } catch (DataIOException ex) {
      Logger.getLogger(Example1.class.getName()).log(Level.SEVERE, null, ex);
    }


    // -- 3. the renderers and renderer factory ---------------------------

    // Create a default ShapeRenderer
    ShapeRenderer r = new ShapeRenderer(10);
    //LabelRenderer r = new LabelRenderer("id");

    // create a new DefaultRendererFactory
    // This Factory will use the ShapeRenderer for all nodes.
    vis.setRendererFactory(new DefaultRendererFactory(r));

    vis.setValue("graph.nodes", null, VisualItem.SHAPE, new Integer(Constants.SHAPE_ELLIPSE));
    // -- 4. the processing actions ---------------------------------------

    // We must color the nodes of the graph.
    // Notice that we refer to the nodes using the text label for the graph,
    // and then appending ".nodes".  The same will work for ".edges" when we
    // only want to access those items.
    // The ColorAction must know what to color, what aspect of those
    // items to color, and the color that should be used.
    ColorAction fill = new ColorAction("graph.nodes", VisualItem.FILLCOLOR, ColorLib.rgb(200, 200, 255));
    fill.add(VisualItem.FIXED, ColorLib.rgb(255, 100, 100));
    fill.add(VisualItem.HIGHLIGHT, ColorLib.rgb(255, 200, 125));

    // Similarly to the node coloring, we use a ColorAction for the
    // edges
    ColorAction edges = new ColorAction("graph.edges", VisualItem.STROKECOLOR, ColorLib.gray(200));

    // Create an action list containing all color assignments
    // ActionLists are used for actions that will be executed
    // at the same time.
    ActionList color = new ActionList();
    color.add(fill);
    color.add(edges);

    // The layout ActionList recalculates
    // the positions of the nodes.
    ActionList layout = new ActionList();

    // We add the layout to the layout ActionList, and tell it
    // to operate on the "graph".
    layout.add(new RandomLayout("graph"));

    // We add a RepaintAction so that every time the layout is
    // changed, the Visualization updates it's screen.
    layout.add(new RepaintAction());

    // add the actions to the visualization
    vis.putAction("color", color);
    vis.putAction("layout", layout);


    ActionList animate = new ActionList();
    animate.add(fill);
    animate.add(new RepaintAction());
    vis.putAction("animate", animate);

    // -- 5. the display and interactive controls -------------------------

    // Create the Display object, and pass it the visualization that it
    // will hold.
    Display d = new Display(vis);

    // Set the size of the display.
    d.setSize(1400, 700);

    // We use the addControlListener method to set up interaction.

    // The DragControl is a built in class for manually moving
    // nodes with the mouse.
    d.addControlListener(new DragControl());
    // Pan with left-click drag on background
    d.addControlListener(new PanControl());
    // Zoom with right-click drag
    d.addControlListener(new ZoomControl());
    d.addControlListener(new NeighborHighlightControl("animate"));

    // -- 6. launch the visualization -------------------------------------


    // The following is standard java.awt.
    // A JFrame is the basic window element in awt.
    // It has a menu (minimize, maximize, close) and can hold
    // other gui elements.

    // Create a new window to hold the visualization.
    // We pass the text value to be displayed in the menubar to the constructor.
    JFrame frame = new JFrame("prefuse example");

    // Ensure application exits when window is closed
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    // The Display object (d) is a subclass of JComponent, which
    // can be added to JFrames with the add method.
    frame.add(d);

    // Prepares the window.
    frame.pack();

    // Shows the window.
    frame.setVisible(true);

    // We have to start the ActionLists that we added to the visualization
    vis.run("color");
    vis.run("layout");
    vis.runAfter("layout", "animate");
  }
}