package prefuse.demos;

import javax.swing.JFrame;

import prefuse.Display;
import prefuse.Visualization;
import prefuse.action.ActionList;
import prefuse.action.RepaintAction;
import prefuse.action.assignment.ColorAction;
import prefuse.action.layout.graph.HierarchicalLayout;
import prefuse.controls.DragControl;
import prefuse.controls.NeighborHighlightControl;
import prefuse.controls.PanControl;
import prefuse.controls.ZoomControl;
import prefuse.data.Graph;
import prefuse.data.io.DataIOException;
import prefuse.data.io.GraphMLReader;
import prefuse.data.io.GraphReader;
import prefuse.render.DefaultRendererFactory;
import prefuse.render.LabelRenderer;
import prefuse.util.ColorLib;
import prefuse.visual.VisualItem;

public class Example2 {
  private static final String $Graph = Example2.class.getResource("/data/array-unsigned-int-array_unsigned_int.graphml").getFile();
  private static final String nodes = "graph.nodes";
  private static final String edges = "graph.edges";

  public static void main (String[] argv) throws DataIOException {

    // -- 1. load the data ------------------------------------------------

    // Here we are manually creating the data structures.  100 nodes are
    // added to the Graph structure.  100 edges are made randomly
    // between the nodes.

    GraphReader reader = new GraphMLReader();
    Graph graph = reader.readGraph($Graph);

    // -- 2. the visualization --------------------------------------------
    // We must first creat the Visualization object.
    Visualization vis = new Visualization();

    // Now we add our previously created Graph object to the visualization.
    // The graph gets a textual label so that we can refer to it later on.
    vis.add("graph", graph);


    // -- 3. the renderers and renderer factory ---------------------------

    // Create a default ShapeRenderer
    //ShapeRenderer r = new ShapeRenderer(10);
    LabelRenderer r = new LabelRenderer("label");
    r.setRoundedCorner(8, 8);

    // create a new DefaultRendererFactory
    // This Factory will use the ShapeRenderer for all nodes.
    vis.setRendererFactory(new DefaultRendererFactory(r));

    ColorAction fill = new ColorAction(nodes,
        VisualItem.FILLCOLOR, ColorLib.rgb(200, 200, 255));
    fill.add(VisualItem.FIXED, ColorLib.rgb(255, 100, 100));
    fill.add(VisualItem.HIGHLIGHT, ColorLib.rgb(255, 200, 125));

    ActionList draw = new ActionList();
    draw.add(fill);
    draw.add(new ColorAction(nodes, VisualItem.STROKECOLOR, 0));
    draw.add(new ColorAction(nodes, VisualItem.TEXTCOLOR, ColorLib.rgb(0, 0, 0)));
    draw.add(new ColorAction(edges, VisualItem.FILLCOLOR, ColorLib.gray(200)));
    draw.add(new ColorAction(edges, VisualItem.STROKECOLOR, ColorLib.gray(200)));

//    vis.setValue("graph.nodes", null, VisualItem.SHAPE, new Integer(Constants.SHAPE_ELLIPSE));
    // -- 4. the processing actions ---------------------------------------

    // The layout ActionList recalculates
    // the positions of the nodes.
    ActionList layout = new ActionList();

    // We add the layout to the layout ActionList, and tell it
    // to operate on the "graph".
    //layout.add(new NodeLinkTreeLayout("graph", 2, 50, 5, 25));
    //layout.add(new RandomLayout("graph"));
    layout.add(new HierarchicalLayout("graph"));

    // We add a RepaintAction so that every time the layout is
    // changed, the Visualization updates it's screen.
    layout.add(new RepaintAction());

    // add the actions to the visualization
    vis.putAction("draw", draw);
    vis.putAction("layout", layout);


    ActionList animate = new ActionList();
    animate.add(fill);
    animate.add(new RepaintAction());
    vis.putAction("animate", animate);

    // -- 5. the display and interactive controls -------------------------

    // Create the Display object, and pass it the visualization that it
    // will hold.
    Display d = new Display(vis);

    // Set the size of the display.
    d.setSize(1800, 1000);
    d.setHighQuality(true);

    // We use the addControlListener method to set up interaction.

    // The DragControl is a built in class for manually moving
    // nodes with the mouse.
    d.addControlListener(new DragControl());
    // Pan with left-click drag on background
    d.addControlListener(new PanControl());
    // Zoom with right-click drag
    d.addControlListener(new ZoomControl());
    d.addControlListener(new NeighborHighlightControl("animate"));

    // -- 6. launch the visualization -------------------------------------


    // The following is standard java.awt.
    // A JFrame is the basic window element in awt.
    // It has a menu (minimize, maximize, close) and can hold
    // other gui elements.

    // Create a new window to hold the visualization.
    // We pass the text value to be displayed in the menubar to the constructor.
    JFrame frame = new JFrame("prefuse example");

    // Ensure application exits when window is closed
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    // The Display object (d) is a subclass of JComponent, which
    // can be added to JFrames with the add method.
    frame.add(d);

    // Prepares the window.
    frame.pack();

    // Shows the window.
    frame.setVisible(true);

    // We have to start the ActionLists that we added to the visualization
    vis.run("draw");
    vis.run("layout");
    vis.runAfter("layout", "animate");
  }
}